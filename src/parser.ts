import { Clause } from './api'

export function parseQuery(query: string): Clause[] {
  let clauses = parseChars(query)

  for (const clause of clauses) {
    clause.phrase = clause.phrase?.toLowerCase()

    if (clause.field_name) {
      clause.field_name = clause.field_name.toLowerCase()

      clause.field_name = FIELD_NAMES.get(clause.field_name)
      if (!clause.field_name) {
        delete clause.field_name
      }
    }
  }

  clauses = removeStopWords(clauses)
  clauses = rewriteNegations(clauses)
  clauses = rewriteConjunctions(clauses)

  // At least one should is required
  if (clauses.length && !clauses.find((clause) => clause.occur != 'MustNot')) {
    clauses.push({})
  }

  return clauses
}

export function writeQuery(clauses: Clause[]): string {
  return clauses
    .map((clause) => {
      const occur = clause.occur ? OCCUR.get(clause.occur) : ''

      const fieldName = clause.field_name
        ? `${INVERTED_FIELD_NAMES.get(clause.field_name)}:`
        : ''

      const phrase =
        clause.phrase && SPACE.test(clause.phrase)
          ? `"${clause.phrase}"`
          : clause.phrase

      return `${occur}${fieldName}${phrase}`
    })
    .join(' ')
}

const FIELD_NAMES = new Map<string, string>([
  ['titel', 'title'],
  ['beschreibung', 'description'],
  ['organisation', 'organisation'],
  ['person', 'person'],
  ['region', 'region'],
  ['schlagwort', 'tags'],
  ['geonames-id', 'geonames_id'],
  ['regionalschlüssel', 'regional_key'],
  ['flussgebiet-id', 'watershed_id'],
  ['cas-rn', 'cas_rn'],
])

const INVERTED_FIELD_NAMES = new Map<string, string>()
FIELD_NAMES.forEach((value, key) => {
  INVERTED_FIELD_NAMES.set(value, key)
})

export const MATCHING_FIELD_LABELS = new Map<string, string>([
  ['geonames_id', 'GeoNames-ID'],
  ['regional_key', 'amtl. Regionalschlüssel'],
  ['watershed_id', 'Flussgebiet-ID'],
  ['cas_rn', 'CAS-Registriernummer'],
])

const OCCUR = new Map<string, string>([
  ['Should', ''],
  ['Must', '+'],
  ['MustNot', '-'],
])

const SPACE = /\s/

const testPhrase = (clause: Clause, pattern: RegExp): boolean =>
  clause.phrase ? pattern.test(clause.phrase) : false

const isNot = (clause: Clause) => testPhrase(clause, /^nicht$|^not$/)

const isAnd = (clause: Clause) =>
  testPhrase(clause, /^und$|^and$|^&$|^sowie$|^in$|^bei$|^im$|^auf$|^am$|^an$/)

const removeStopWords = (clauses: Clause[]) =>
  clauses.filter(
    (clause) =>
      !testPhrase(
        clause,
        /^der$|^die$|^das$|^dem$|^den$|^ein$|^eine$|^einen$|^einer$|^eines$|^des$|^dessen$|^deren$/,
      ),
  )

function setMust(clause: Clause) {
  if (clause.occur !== 'MustNot') {
    clause.occur = 'Must'
  }
}

function rewriteNegations(clauses: Clause[]): Clause[] {
  const newClauses: Clause[] = []

  for (let idx = 0; idx < clauses.length; idx++) {
    let clause = clauses[idx]

    if (idx + 1 < clauses.length && isNot(clause)) {
      idx++
      clause = clauses[idx]

      clause.occur = 'MustNot'

      const prevClause = newClauses.at(-1)

      if (prevClause && !isAnd(prevClause)) {
        setMust(prevClause)
      }
    }

    newClauses.push(clause)
  }

  return newClauses
}

function rewriteConjunctions(clauses: Clause[]): Clause[] {
  const newClauses: Clause[] = []

  for (let idx = 0; idx < clauses.length; idx++) {
    let clause = clauses[idx]

    if (idx + 1 < clauses.length && isAnd(clause)) {
      const occur = clause.occur

      idx++
      clause = clauses[idx]

      if (occur === 'MustNot') {
        clause.occur = occur
      } else {
        setMust(clause)
      }

      const prevClause = newClauses.at(-1)

      if (prevClause) {
        setMust(prevClause)
      }
    }

    newClauses.push(clause)
  }

  return newClauses
}

function parseChars(query: string): Clause[] {
  const clauses: Clause[] = []
  const chars = query[Symbol.iterator]()

  let occur: 'Should' | 'Must' | 'MustNot' | undefined = undefined
  let phrase: string = ''
  let field_name: string | undefined = undefined

  let state: 'Initial' | 'Word' | 'Quoted' = 'Initial'

  const pushClause = () => {
    // Default to must for quoted phrase
    if (occur === undefined && state === 'Quoted') {
      occur = 'Must'
    }

    clauses.push({
      occur,
      phrase,
      field_name,
    })

    occur = undefined
    phrase = ''
    field_name = undefined
  }

  for (;;) {
    const char = chars.next()
    if (char.done) {
      break
    }

    if (char.value.match(SPACE)) {
      switch (state) {
        case 'Initial':
          if (field_name === undefined) {
            occur = undefined
          }
          break
        case 'Word':
          // Word ends at space
          pushClause()

          state = 'Initial'
          break
        case 'Quoted':
          // Quoted consumes space
          phrase += char.value
          break
        default:
          unhandledState(state)
      }
    } else if (char.value === '"') {
      switch (state) {
        case 'Initial':
          // Start parsing quoted
          phrase = ''

          state = 'Quoted'
          break
        case 'Word':
          // Fix up delayed quotes
          state = 'Quoted'
          break
        case 'Quoted':
          // End parsing quoted
          pushClause()

          state = 'Initial'
          break
        default:
          unhandledState(state)
      }
    } else if (char.value === ':') {
      switch (state) {
        case 'Initial':
          // Just ignore stray colon
          break
        case 'Word':
          // Word until here was field name
          field_name = phrase

          state = 'Initial'
          break
        case 'Quoted':
          // Quoted consumes colon
          phrase += char.value
          break
        default:
          unhandledState(state)
      }
    } else {
      switch (state) {
        case 'Initial':
          // First word character or modifier
          if (char.value === '+') {
            occur = 'Must'
          } else if (char.value === '-') {
            occur = 'MustNot'
          } else if (char.value === '?') {
            occur = 'Should'
          } else {
            phrase = char.value

            state = 'Word'
          }
          break
        case 'Word':
          // Word consumes character
          phrase += char.value
          break
        case 'Quoted':
          // Quoted consumes character
          phrase += char.value
          break
        default:
          unhandledState(state)
      }
    }
  }

  switch (state) {
    case 'Initial':
      // Nothing to be done
      break
    case 'Word':
    case 'Quoted':
      // Just finish the word
      // or quote as if closed
      pushClause()
      break
    default:
      unhandledState(state)
  }

  return clauses
}

function unhandledState(state: never): never {
  throw state
}
