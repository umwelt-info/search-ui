import { expect, it, describe } from 'vitest'

import { computePaginationOptions } from './utils'

describe('computeOptions', () => {
  it('has no options when there are no pages', async () => {
    expect(computePaginationOptions(1, 0)).toEqual([])
  })

  it('shows all pages when there are less or equal than 5 pages', async () => {
    expect(computePaginationOptions(1, 3)).toEqual([1, 2, 3])
  })

  it('shows the first 5 pages when there are more than 5 pages and the current page is less than 3', async () => {
    expect(computePaginationOptions(2, 10)).toEqual([1, 2, 3, 4, 5])
  })

  it('shows the last 5 pages when currentPage is at most 2 smaller than max page', async () => {
    expect(computePaginationOptions(9, 10)).toEqual([6, 7, 8, 9, 10])
  })

  it('shows the previous 2 and next 2 pages when currentPage is at most 2 smaller than max page', async () => {
    expect(computePaginationOptions(6, 10)).toEqual([4, 5, 6, 7, 8])
  })
})
