import React from 'react'
import ReactDOM from 'react-dom/client'

import App from './components/App'
import HomepageApp from './components/HomepageApp'

const rootElement = document.getElementById('search-ui-root')
if(rootElement) {
  ReactDOM.createRoot(rootElement).render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
  )
} else {
  const rootElement = document.getElementById('homepage-search-ui-root')
  if(rootElement) {
    ReactDOM.createRoot(rootElement).render(
      <React.StrictMode>
          <HomepageApp />
      </React.StrictMode>,
    )
  }
}
