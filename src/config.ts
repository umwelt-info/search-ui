declare global {
  export interface Window {
    API_ENDPOINT: string
  }
}

export const RESULTS_PER_PAGE = 7

export const MAX_COMPLETIONS = 10

export const MAX_REPLACEMENTS = 7

export const MAX_ROOTS = 6
