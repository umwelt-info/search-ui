import { Type } from './api'

export function buildTypeLabel(types: Type): string {
  if (typeof types === 'object')
    if ('Taxon' in types) {
      return 'Taxon'
    } else if ('Measurements' in types) {
      const measurements = types.Measurements
      if (measurements.station && measurements.station.id) {
        return 'Messstelle'
      } else {
        return 'Messwerte'
      }
    } else if (typeof types === 'object' && 'Text' in types) {
      const text_type = types.Text.text_type
      return TEXT_TYPE_LABELS[text_type]
    } else if ('ChemicalCompound' in types) {
      return 'Chemische Verbindung'
    }

  return TYPE_LABELS[types]
}

const TYPE_LABELS: { [key: string]: string } = {
  Event: 'Ereignis',
  LearningResource: 'Lehrmaterial',
  News: 'Neuigkeit',
  PressRelease: 'Veröffentlichung',
  Editorial: 'Artikel',
  BlogPost: 'Blogbeitrag',
  SupportProgram: 'Förderprogramm',
  MapService: 'Kartendienst',
  Uvp: 'Umweltprüfung',
  Software: 'Software',
  Image: 'Bildmaterial',
  Dataset: 'Strukturierter Datensatz',
  Legal: 'Gesetzestext',
  Video: 'Video',
  Audio: 'Audio',
  Form: 'Formular',
}

const TEXT_TYPE_LABELS: { [key: string]: string } = {
  Unspecified: 'Text',
  OfficialFile: 'Akte',
  Manual: 'Handbuch',
  Report: 'Bericht',
  Publication: 'Veröffentlichung',
  BlogPost: 'Blogbeitrag',
  PressRelease: 'Pressemitteilung',
  News: 'Nachrichten',
  Editorial: 'Redaktioneller Beitrag',
  Booklet: 'Broschüre',
}

export const ROOT_LABELS: { [key: string]: string } = {
  typesRoot: 'Typ',
  topicsRoot: 'Schlagwörter',
  originsRoot: 'Quelle',
  licensesRoot: 'Lizenz',
  languagesRoot: 'Sprache',
  resourceTypesRoot: 'Ressourcentyp',
}

export const STATUS_LABELS: { [key: string]: string } = {
  Active: 'Aktiv',
  Obsolete: 'Veraltet',
  Planned: 'Geplant',
  UnderDevelopment: 'In Entwicklung',
}

export const DOMAIN_LABELS: { [key: string]: string } = {
  Air: 'Luft',
  Rivers: 'Fluss',
  Groundwater: 'Grundwasser',
  Geophysics: 'Geophysik',
  Radioactivity: 'Radioaktivität',
  Surfacewater: 'Oberflächenwasser',
  Sea: 'Meer',
  Chemistry: 'Chemie',
  Biology: 'Biologie',
  Soil: 'Boden',
  Water: 'Wasser',
}

export type ReportingObligation = {
  label: string
  href: string
}

export const REPORTING_OBLIGATIONS: { [key: string]: ReportingObligation } = {
  Wrrl: {
    label: 'Wasserrahmenrichtlinie',
    href: 'https://www.bmuv.de/themen/wasser-und-binnengewaesser/gewaesserschutzpolitik/deutschland/umsetzung-der-wrrl-in-deutschland',
  },
  Ospar: {
    label: 'Übereinkommen Oslo/Paris zur Überwachung der Nordsee',
    href: 'https://www.bfn.de/abkommen-richtlinie/uebereinkommen-zum-schutz-der-meeresumwelt-des-nordost-atlantiks-oslo-paris',
  },
  Helcom: {
    label: 'Helsinki-Konvention zur Überwachung der Ostsee',
    href: 'https://helcom.fi/',
  },
  SoeReport: {
    label:
      'Umweltberichterstattung Meer (State of Environment Report Waterbase) WISE-6',
    href: 'https://rod.eionet.europa.eu/obligations/14',
  },
  Nitrat: {
    label:
      'Richtlinie 91/676/EWG zum Schutz der Gewässer vor Verunreinigung durch Nitrat aus landwirtschaftlichen Quellen',
    href: 'https://eur-lex.europa.eu/DE/legal-content/summary/fighting-water-pollution-from-agricultural-nitrates.html',
  },
  Prtr: {
    label: 'Pollutant Release and Transfer Register',
    href: 'https://www.umweltbundesamt.de/themen/prtr-mehr-transparenz-durch-verbesserte',
  },
}
