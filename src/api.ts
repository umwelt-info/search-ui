export type Dataset = {
  source: string
  id: string
  origins: string[]
  title: string
  description?: string
  quality: Quality
  types?: Type[]
  language: {
    label: string
  }
  license: {
    label: string
    url?: string
  }
  mandatory_registration?: boolean
  tags?: Tag[]
  source_url: string
  source_url_explainer?: SourceUrlExplainer
  bounding_boxes?: BoundingBox[]
  time_ranges?: TimeRange[]
  status: string
  resources?: Resource[]
  alternatives?: Alternative[]
  issued?: string
  modified?: string
}

export type Quality = {
  accessibility: {
    landing_page: 'Generic' | 'Specific'
    direct_access: boolean
    landing_page_score: number
    publicly_accessible: boolean
    score: number
  }
  findability: {
    description: number
    title: number
    identifier: boolean
    keywords: number
    score: number
    spatial_score: number
    temporal: number
  }
  interoperability: {
    machine_readable_data: boolean
    machine_readable_metadata: boolean
    media_type: boolean
    open_file_format: boolean
    score: number
  }
  reusability: {
    contact_info: boolean
    license_score: number
    publisher_info: boolean
    score: number
  }
}

export type Type =
  | string
  | {
      Taxon: {
        common_names: string[]
        scientific_name: string
      }
    }
  | {
      Measurements: {
        domain: string
        measured_variables: string[]
        methods: string[]
        station?: {
          id?: string
          measurement_frequency?: string
          reporting_obligations?: string[]
        }
      }
    }
  | { Text: { text_type: string } }
  | {
      ChemicalCompound: {
        name: string
        synonyms: string[]
        cas_registry_number: string | null
      }
    }

export type Tag = { Other: string } | { Umthes: { id: number; label: string } }

export type SourceUrlExplainer =
  | {
      Informational: string
    }
  | {
      CopyTitle: string
    }
  | {
      CopyStationId: string
    }

export type BoundingBox = {
  min: { x: number; y: number }
  max: { x: number; y: number }
}

export type TimeRange = {
  from: string
  until: string
}

export type Resource = {
  description?: string
  type: {
    label: string
    path: string
  }
  url: string
  direct_link: boolean
  primary_content: boolean
}

export type Alternative =
  | {
      Source: {
        source: string
        title: string
        url: string
      }
    }
  | {
      Language: {
        language: string
        url: string
      }
    }

export type Origin = {
  name: string
  about_url: string | null
  contact_url: string
  email: string | null
  description: string
}

export type Clause = {
  occur?: 'Should' | 'Must' | 'MustNot'
  field_name?: string
  phrase?: string
}

export type SearchParams = {
  query: Clause[]
  page: number | undefined
  results_per_page: number
  types_root: string | undefined
  topics_root: string | undefined
  origins_root: string | undefined
  licenses_root: string | undefined
  languages_root: string | undefined
  resource_types_root: string | undefined
  bounding_box_south: number | undefined
  bounding_box_west: number | undefined
  bounding_box_north: number | undefined
  bounding_box_east: number | undefined
  time_range_from: string | undefined
  time_range_until: string | undefined
}

export type SearchResponse = {
  count: number | undefined
  pages: number
  results: Dataset[]
  types: [string, number][]
  topics: [string, number][]
  origins: [string, number][]
  licenses: [string, number][]
  languages: [string, number][]
  resource_types: [string, number][]
  similar_terms: [string, string][]
  related_terms: [string, string[]][]
}

export const EMPTY_SEARCH_RESPONSE = {
  count: undefined,
  pages: 0,
  results: [],
  types: [],
  topics: [],
  origins: [],
  licenses: [],
  languages: [],
  resource_types: [],
  related_terms: [],
  similar_terms: [],
}

export async function fetchSearch(params: SearchParams) {
  const url = new URL('/search', window.API_ENDPOINT)

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params),
  })

  const body = await response.json()

  return body as SearchResponse
}

export type CompleteParams = {
  query: Clause[]
}

export type CompleteResponse = {
  completions: [string, number][]
  similar_terms: [string, number][]
  matching_fields: [string, number][]
  types: [string, number][]
  topics: [string, number][]
  origins: [string, number][]
  licenses: [string, number][]
  languages: [string, number][]
  resource_types: [string, number][]
}

export const EMPTY_COMPLETE_RESPONSE = {
  completions: [],
  similar_terms: [],
  matching_fields: [],
  types: [],
  topics: [],
  origins: [],
  licenses: [],
  languages: [],
  resource_types: [],
}

export async function fetchComplete(params: CompleteParams) {
  const url = new URL('/complete', window.API_ENDPOINT)

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params),
  })

  const body = await response.json()

  return body as CompleteResponse
}

async function fetchDetail(detail: string) {
  const url = new URL(`/dataset/${detail}`, window.API_ENDPOINT)

  const response = await fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
    },
  })

  const body = await response.json()

  return body as Dataset
}

type OriginResponse = {
  id: string
  origin: Origin
}

async function fetchOrigin(id: string) {
  const url = new URL(`/origin${id}`, window.API_ENDPOINT)

  const response = await fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
    },
  })

  const body = await response.json()

  return body as OriginResponse
}

export async function fetchDetailAndOrigins(
  detail: string,
): Promise<[Dataset, Origin[]]> {
  const dataset = await fetchDetail(detail)

  // Deduplicate origins which are sometimes available under multiple ID.
  const origins: Origin[] = []

  for (const originRequest of dataset.origins.map((id) => fetchOrigin(id))) {
    try {
      const originResponse = await originRequest

      if (
        !origins.find(
          (otherOrigin) => otherOrigin.name === originResponse.origin.name,
        )
      ) {
        origins.push(originResponse.origin)
      }
    } catch (error) {
      console.error(`Failed to fetch origin: ${error}`)
    }
  }

  return [dataset, origins]
}
