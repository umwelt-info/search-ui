import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

import {
  Dataset,
  Origin,
  Clause,
  SearchResponse,
  EMPTY_SEARCH_RESPONSE,
  fetchSearch,
  CompleteResponse,
  EMPTY_COMPLETE_RESPONSE,
  fetchComplete,
  fetchDetailAndOrigins,
} from './api'
import { parseQuery } from './parser'
import { RESULTS_PER_PAGE } from './config'

export type AnyRoot =
  | 'typesRoot'
  | 'topicsRoot'
  | 'originsRoot'
  | 'licensesRoot'
  | 'languagesRoot'
  | 'resourceTypesRoot'

export interface Store {
  query: string
  searchedQuery: Clause[]
  completedQuery: Clause[]
  navigatedQuery: string

  page: number

  typesRoot: string
  searchedTypesRoot: string
  topicsRoot: string
  searchedTopicsRoot: string
  originsRoot: string
  searchedOriginsRoot: string
  licensesRoot: string
  searchedLicensesRoot: string
  languagesRoot: string
  searchedLanguagesRoot: string
  resourceTypesRoot: string
  searchedResourceTypesRoot: string

  boundingBox: [number, number, number, number] | null

  timeRangeFrom: string | null
  timeRangeUntil: string | null

  detail: string | null

  results: SearchResponse
  completions: CompleteResponse
  dataset: Dataset | null
  origins: Origin[]

  searchPending: boolean
  searchStale: boolean

  completePending: boolean
  completeStale: boolean

  detailPending: boolean
  detailStale: boolean

  mobileFilterDrawerOpen: boolean
}

export const useStore = create<Store>()(
  persist(
    (_set) => ({
      query: '',
      navigatedQuery: '',
      searchedQuery: [],
      completedQuery: [],

      page: 1,

      typesRoot: '/',
      searchedTypesRoot: '/',
      topicsRoot: '/',
      searchedTopicsRoot: '/',
      originsRoot: '/',
      searchedOriginsRoot: '/',
      licensesRoot: '/',
      searchedLicensesRoot: '/',
      languagesRoot: '/',
      searchedLanguagesRoot: '/',
      resourceTypesRoot: '/',
      searchedResourceTypesRoot: '/',

      boundingBox: null,

      timeRangeFrom: null,
      timeRangeUntil: null,

      detail: null,

      results: EMPTY_SEARCH_RESPONSE,
      completions: EMPTY_COMPLETE_RESPONSE,
      dataset: null,
      origins: [],

      searchPending: false,
      searchStale: false,

      completePending: false,
      completeStale: false,

      detailPending: false,
      detailStale: false,

      mobileFilterDrawerOpen: false,
    }),
    {
      onRehydrateStorage: () => {
        setTimeout(() => {
          triggerSearch()
          triggerComplete()
          triggerDetail()
        })
      },
      name: 'search-ui',
      partialize: (state) => ({
        query: state.navigatedQuery,
        page: state.page,
        typesRoot: state.typesRoot,
        topicsRoot: state.topicsRoot,
        originsRoot: state.originsRoot,
        licensesRoot: state.licensesRoot,
        languagesRoot: state.languagesRoot,
        resourceTypesRoot: state.resourceTypesRoot,
        boundingBox: state.boundingBox,
        timeRangeFrom: state.timeRangeFrom,
        timeRangeUntil: state.timeRangeUntil,
        detail: state.detail,
      }),
      storage: createJSONStorage(() => ({
        getItem: (key): string => {
          console.assert(key === 'search-ui')

          const params = new URLSearchParams(window.location.search)

          let query = params.get('query') ?? ''

          const tags = params.get('tags') ?? ''
          if (tags && query) query += ` +schlagwort:"${tags}"`
          else if (tags) query = `+schlagwort:"${tags}"`

          const page = parseInt(params.get('page') ?? '1')
          const typesRoot = parseRoot(params, 'types')
          const topicsRoot = parseRoot(params, 'topics')
          const originsRoot = parseRoot(params, 'origins')
          const licensesRoot = parseRoot(params, 'licenses')
          const languagesRoot = parseRoot(params, 'languages')
          const resourceTypesRoot = parseRoot(params, 'resourceTypes')

          const boundingBox = parseBoundingBox(params)

          const timeRangeFrom = params.get('timeRangeFrom')
          const timeRangeUntil = params.get('timeRangeUntil')

          const detail = params.get('detail')

          const state = {
            query,
            navigatedQuery: query,
            page,
            typesRoot,
            topicsRoot,
            originsRoot,
            licensesRoot,
            languagesRoot,
            resourceTypesRoot,
            boundingBox,
            timeRangeFrom,
            timeRangeUntil,
            detail,
          }

          return JSON.stringify({ state, version: 0 })
        },
        setItem: (key, value): void => {
          console.assert(key === 'search-ui')

          const state = JSON.parse(value).state

          const url = new URL(window.location.href)

          setParams(url.searchParams, state)

          if (url.toString() !== window.location.href) {
            window.history.pushState({}, '', url)
          }
        },
        removeItem: (key): void => {
          console.assert(key === 'search-ui')

          const url = new URL(window.location.href)

          if (url.search !== '') {
            url.search = ''

            window.history.pushState({}, '', url)
          }
        },
      })),
    },
  ),
)

window.addEventListener('popstate', () => useStore.persist.rehydrate())

export function setQuery(query: string) {
  useStore.setState({ query })
}

export function setPage(page: number) {
  useStore.setState({ page })
}

export function setAnyRoot(key: AnyRoot, value: string) {
  useStore.setState({ [key]: value })
}

export function setDetail(detail: string | null) {
  useStore.setState({ detail })
}

export function setTimeRangeFrom(timeRangeFrom: string | null) {
  useStore.setState({ timeRangeFrom })
}

export function setTimeRangeUntil(timeRangeUntil: string | null) {
  useStore.setState({ timeRangeUntil })
}

export function setBoundingBox(
  boundingBox: [number, number, number, number] | null,
) {
  useStore.setState({ boundingBox })
}

export function triggerSearch(homepage?: boolean) {
  useStore.setState((store) => {
    if (homepage) {
      const url = new URL('/suche', window.location.origin)
      setParams(url.searchParams, store)
      window.location.href = url.toString()
    }

    if (store.searchPending) {
      return { searchStale: true }
    }

    const navigatedQuery = store.query
    const searchedQuery = parseQuery(store.query)
    const searchedTypesRoot = store.typesRoot
    const searchedTopicsRoot = store.topicsRoot
    const searchedOriginsRoot = store.originsRoot
    const searchedLicensesRoot = store.licensesRoot
    const searchedLanguagesRoot = store.languagesRoot
    const searchedResourceTypesRoot = store.resourceTypesRoot

    const params = {
      query: searchedQuery,
      page: store.page !== 1 ? store.page : undefined,
      results_per_page: RESULTS_PER_PAGE,
      types_root: filterRoot(searchedTypesRoot),
      topics_root: filterRoot(searchedTopicsRoot),
      origins_root: filterRoot(searchedOriginsRoot),
      licenses_root: filterRoot(searchedLicensesRoot),
      languages_root: filterRoot(searchedLanguagesRoot),
      resource_types_root: filterRoot(searchedResourceTypesRoot),
      bounding_box_south: store.boundingBox?.[0],
      bounding_box_west: store.boundingBox?.[1],
      bounding_box_north: store.boundingBox?.[2],
      bounding_box_east: store.boundingBox?.[3],
      time_range_from: store.timeRangeFrom ?? undefined,
      time_range_until: store.timeRangeUntil ?? undefined,
    }

    const updateResults = (results: SearchResponse) =>
      useStore.setState((store) => {
        // Limit the page if it is out of bounds
        let page = store.page

        if (results && results.pages && results.pages < page) {
          page = results.pages
        }

        if (store.searchStale) {
          setTimeout(() => triggerSearch())
        }

        return {
          page,
          navigatedQuery,
          searchedQuery,
          searchedTypesRoot,
          searchedTopicsRoot,
          searchedOriginsRoot,
          searchedLicensesRoot,
          searchedLanguagesRoot,
          searchedResourceTypesRoot,
          results,
          searchPending: false,
        }
      })

    fetchSearch(params).then(
      (results) => updateResults(results),
      (error) => {
        console.error(`Failed to fetch search results: ${error}`)
        updateResults(EMPTY_SEARCH_RESPONSE)
      },
    )

    return { searchPending: true, searchStale: false }
  })
}

export function triggerComplete() {
  useStore.setState((store) => {
    if (store.completePending) {
      return { completeStale: true }
    }

    const completedQuery = parseQuery(store.query)

    const params = {
      query: completedQuery,
    }

    const lastClause = completedQuery.at(-1)
    if (!lastClause || (lastClause.phrase && lastClause.phrase.length < 3)) {
      return {
        completedQuery,
        completions: EMPTY_COMPLETE_RESPONSE,
        completePending: false,
        completeStale: false,
      }
    }

    const updateCompletions = (completions: CompleteResponse) =>
      useStore.setState((store) => {
        if (store.completeStale) {
          setTimeout(() => triggerComplete())
        }

        return { completedQuery, completions, completePending: false }
      })

    fetchComplete(params).then(
      (results) => updateCompletions(results),
      (error) => {
        console.error(`Failed to fetch completions: ${error}`)
        updateCompletions(EMPTY_COMPLETE_RESPONSE)
      },
    )

    return { completePending: true, completeStale: false }
  })
}

export function triggerDetail() {
  useStore.setState((store) => {
    if (store.detailPending) {
      return { detailStale: true }
    }

    if (!store.detail) {
      return { dataset: null, detailPending: false, detailStale: false }
    }

    const updateDataset = (dataset?: Dataset, origins?: Origin[]) =>
      useStore.setState((store) => {
        if (store.detailStale) {
          setTimeout(() => triggerDetail())
        }

        return { dataset, origins, detailPending: false }
      })

    fetchDetailAndOrigins(store.detail).then(
      ([dataset, origins]) => updateDataset(dataset, origins),
      (error) => {
        console.error(`Failed to fetch details: ${error}`)
        updateDataset()
      },
    )

    return { detailPending: true, detailStale: false }
  })
}

export function toggleMobileFilterDrawer() {
  useStore.setState((store) => {
    const open = !store.mobileFilterDrawerOpen

    // prevent body scrolling when drawer is open
    document.body.style.overflow = open ? 'hidden' : 'auto'

    return { mobileFilterDrawerOpen: open }
  })
}

function setParams(
  params: URLSearchParams,
  /* eslint-disable @typescript-eslint/no-explicit-any */ state: any,
) {
  if (state.query !== '') {
    params.set('query', state.query)
  } else {
    params.delete('query')
  }

  params.delete('tags')

  if (state.page !== 1) {
    params.set('page', state.page)
  } else {
    params.delete('page')
  }

  setRoot(params, 'types', state.typesRoot)
  setRoot(params, 'topics', state.topicsRoot)
  setRoot(params, 'origins', state.originsRoot)
  setRoot(params, 'licenses', state.licensesRoot)
  setRoot(params, 'languages', state.languagesRoot)
  setRoot(params, 'resourceTypes', state.resourceTypesRoot)

  insertBoundingBox(params, state.boundingBox)

  setNullable(params, 'timeRangeFrom', state.timeRangeFrom)
  setNullable(params, 'timeRangeUntil', state.timeRangeUntil)

  setNullable(params, 'detail', state.detail)
}

function filterRoot(value: string): string | undefined {
  return value !== '/' ? value : undefined
}

function parseRoot(params: URLSearchParams, key: string): string {
  const value = params.get(key) ?? ''
  return '/' + value.split('\\').join('/')
}

function setRoot(params: URLSearchParams, key: string, value: string) {
  if (value !== '/') {
    params.set(key, value.slice(1).split('/').join('\\'))
  } else {
    params.delete(key)
  }
}

function parseBoundingBox(
  params: URLSearchParams,
): [number, number, number, number] | null {
  if (
    params.has('boundingBoxSouth') ||
    params.has('boundingBoxWest') ||
    params.has('boundingBoxNorth') ||
    params.has('boundingBoxEast')
  ) {
    const south = parseFloat(params.get('boundingBoxSouth') ?? '-90')
    const west = parseFloat(params.get('boundingBoxWest') ?? '-180')
    const north = parseFloat(params.get('boundingBoxNorth') ?? '90')
    const east = parseFloat(params.get('boundingBoxEast') ?? '180')

    return [south, west, north, east]
  } else {
    return null
  }
}

function insertBoundingBox(
  params: URLSearchParams,
  boundingBox: [number, number, number, number] | null,
) {
  if (boundingBox) {
    const [south, west, north, east] = boundingBox

    params.set('boundingBoxSouth', south.toFixed(2))
    params.set('boundingBoxWest', west.toFixed(2))
    params.set('boundingBoxNorth', north.toFixed(2))
    params.set('boundingBoxEast', east.toFixed(2))
  } else {
    params.delete('boundingBoxSouth')
    params.delete('boundingBoxWest')
    params.delete('boundingBoxNorth')
    params.delete('boundingBoxEast')
  }
}

function setNullable(
  params: URLSearchParams,
  key: string,
  value: string | null,
) {
  if (value !== null) {
    params.set(key, value)
  } else {
    params.delete(key)
  }
}
