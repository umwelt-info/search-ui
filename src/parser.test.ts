import { expect, it } from 'vitest'

import { parseQuery, writeQuery } from './parser'

it('can handle an empty query', () => {
  const query = ''
  const result = parseQuery(query)
  expect(result.length).toBe(0)
})

it('can handle a single clause', () => {
  const query = 'Hello'
  const result = parseQuery(query)
  expect(result.length).toBe(1)
  expect(result[0]).toEqual({ phrase: 'hello' })
})

it('can handle simple queries', () => {
  const query = 'Hello World'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ phrase: 'world' })
})

it('space after dash means the dash is ignored', () => {
  const query = 'hello - world'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ phrase: 'world' })
})

it('but if the space follows a field name, it is ignored', () => {
  const query = 'hello -Schlagwort: "MyTag"'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({
    occur: 'MustNot',
    phrase: 'mytag',
    field_name: 'tags',
  })
})

it('can handle negative words', () => {
  const query = 'hello -world'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'world' })
})

it('can handle negative sentences', () => {
  const query = 'hello -"my World"'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'my world' })
})

it('can handle sentences', () => {
  const query = 'hello "my world"'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ occur: 'Must', phrase: 'my world' })
})

it('supports optional sentences', () => {
  const query = 'hello ?"my world"'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ occur: 'Should', phrase: 'my world' })
})

it('is not confused by syntax inside sentences', () => {
  const query = '"hell:o" "-my world"'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'hell:o' })
  expect(result[1]).toEqual({ occur: 'Must', phrase: '-my world' })
})

it('can handle tags', () => {
  const query = 'hello +Schlagwort:"MyTag"'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({
    occur: 'Must',
    phrase: 'mytag',
    field_name: 'tags',
  })
})

it.each([
  ['Region', 'meine Region', 'region'],
  ['Titel', 'mein Titel', 'title'],
  ['Beschreibung', 'meine Beschreibung', 'description'],
  ['Organisation', 'meine Organisation', 'organisation'],
  ['Person', 'meine Person', 'person'],
  ['Schlagwort', 'mein Schlagwort', 'tags'],
])('handles promised field_name "%s"', (field, phrase, expectedFieldName) => {
  const query = `${field}: "${phrase}"`
  const result = parseQuery(query)

  expect(result.length).toBe(1)
  expect(result[0]).toEqual({
    occur: 'Must',
    phrase: phrase.toLowerCase(),
    field_name: expectedFieldName,
  })
})

it('drops unknown field names', () => {
  const query = 'hello +sauerkraut:bratwurst'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({
    occur: 'Must',
    phrase: 'bratwurst',
  })
})

it('supports explicit should words', () => {
  const query = 'hello ?world'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ phrase: 'hello' })
  expect(result[1]).toEqual({ occur: 'Should', phrase: 'world' })
})

it('fixes up all-negative query', () => {
  const query = '-grundwassermessstelle'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({
    occur: 'MustNot',
    phrase: 'grundwassermessstelle',
  })
  expect(result[1]).toEqual({})
})

it('can handle a single und conjunction', () => {
  const query = 'Hello und World'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'hello' })
  expect(result[1]).toEqual({ occur: 'Must', phrase: 'world' })
})

it('can handle a single und conjunction in the middle', () => {
  const query = 'Wasser Boden und Luft Meer'
  const result = parseQuery(query)
  expect(result.length).toBe(4)
  expect(result[0]).toEqual({ phrase: 'wasser' })
  expect(result[1]).toEqual({ occur: 'Must', phrase: 'boden' })
  expect(result[2]).toEqual({ occur: 'Must', phrase: 'luft' })
  expect(result[3]).toEqual({ phrase: 'meer' })
})

it('can handle multiple und conjunctions', () => {
  const query = 'Wasser und Boden und Luft und Meer'
  const result = parseQuery(query)
  expect(result.length).toBe(4)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'wasser' })
  expect(result[1]).toEqual({ occur: 'Must', phrase: 'boden' })
  expect(result[2]).toEqual({ occur: 'Must', phrase: 'luft' })
  expect(result[3]).toEqual({ occur: 'Must', phrase: 'meer' })
})

it.each(['und', 'bei', 'in', '&', 'and', 'sowie', 'auf', 'am', 'an'])(
  'can handle all promised conjunctions: "%s"',
  (conjunction) => {
    const query = `Wasser ${conjunction} Boden`
    const result = parseQuery(query)
    expect(result.length).toBe(2)
    expect(result[0]).toEqual({ occur: 'Must', phrase: 'wasser' })
    expect(result[1]).toEqual({ occur: 'Must', phrase: 'boden' })
  },
)

it('can handle negated conjunctions', () => {
  const query = 'Wasser & Boden und Luft und nicht Meer'
  const result = parseQuery(query)
  expect(result.length).toBe(4)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'wasser' })
  expect(result[1]).toEqual({ occur: 'Must', phrase: 'boden' })
  expect(result[2]).toEqual({ occur: 'Must', phrase: 'luft' })
  expect(result[3]).toEqual({ occur: 'MustNot', phrase: 'meer' })
})

it('can handle negated conjunctions in the middle', () => {
  const query = 'Wasser und nicht Boden und Luft and Meer'
  const result = parseQuery(query)
  expect(result.length).toBe(4)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'wasser' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'boden' })
  expect(result[2]).toEqual({ occur: 'Must', phrase: 'luft' })
  expect(result[3]).toEqual({ occur: 'Must', phrase: 'meer' })
})

it('can handle multiple negated conjunctions', () => {
  const query = 'Wasser und not Boden und Luft und nicht Meer'
  const result = parseQuery(query)
  expect(result.length).toBe(4)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'wasser' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'boden' })
  expect(result[2]).toEqual({ occur: 'Must', phrase: 'luft' })
  expect(result[3]).toEqual({ occur: 'MustNot', phrase: 'meer' })
})

it('can handle localizations', () => {
  const query = 'Messstellen in Hamburg'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'messstellen' })
  expect(result[1]).toEqual({
    occur: 'Must',
    phrase: 'hamburg',
  })
})

it('can handle negated localizations', () => {
  const query = 'Grundwasser nicht in Hamburg'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'grundwasser' })
  expect(result[1]).toEqual({
    occur: 'MustNot',
    phrase: 'hamburg',
  })
})

it('will keep already negated localizations intact', () => {
  const query = 'Grundwasser bei -Hamburg'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'grundwasser' })
  expect(result[1]).toEqual({
    occur: 'MustNot',
    phrase: 'hamburg',
  })
})

it('will not be confused by common stop words', () => {
  const query = 'Fische in der Elbe'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'fische' })
  expect(result[1]).toEqual({
    occur: 'Must',
    phrase: 'elbe',
  })
})

it('can handle localized conjunctions', () => {
  const query = 'Grundwasser und nicht Messstellen bei Hamburg'
  const result = parseQuery(query)
  expect(result.length).toBe(3)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'grundwasser' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'messstellen' })
  expect(result[2]).toEqual({
    occur: 'Must',
    phrase: 'hamburg',
  })
})

it('can handle a leading und', () => {
  const query = 'und Grundwasser'
  const result = parseQuery(query)
  expect(result.length).toBe(1)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'grundwasser' })
})

it('can handle a negation like a negated conjunction', () => {
  const query = 'Grundwasser nicht Oberflächenwasser'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'grundwasser' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'oberflächenwasser' })
})

it('can handle a leading negated conjunction', () => {
  const query = 'und nicht Oberflächenwasser'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'MustNot', phrase: 'oberflächenwasser' })
  expect(result[1]).toEqual({})
})

it.each(['und nicht', 'not'])(
  'can handle all promised negated conjunctions: "%s"',
  (conjunction) => {
    const query = `${conjunction} Oberflächenwasser`
    const result = parseQuery(query)
    expect(result.length).toBe(2)
    expect(result[0]).toEqual({ occur: 'MustNot', phrase: 'oberflächenwasser' })
    expect(result[1]).toEqual({})
  },
)

it('can handle a leading negation', () => {
  const query = 'nicht Oberflächenwasser'
  const result = parseQuery(query)
  expect(result.length).toBe(2)
  expect(result[0]).toEqual({ occur: 'MustNot', phrase: 'oberflächenwasser' })
  expect(result[1]).toEqual({})
})

it('can handle negated elided conjunctions', () => {
  const query = 'Boden und nicht Luft nicht Meer'
  const result = parseQuery(query)
  expect(result.length).toBe(3)
  expect(result[0]).toEqual({ occur: 'Must', phrase: 'boden' })
  expect(result[1]).toEqual({ occur: 'MustNot', phrase: 'luft' })
  expect(result[2]).toEqual({ occur: 'MustNot', phrase: 'meer' })
})

it('can round-trip query rewrites', () => {
  const query = 'Boden +Wasser -"kalte Luft"'
  const result = writeQuery(parseQuery(query))
  expect(result).toEqual('boden +wasser -"kalte luft"')
})
