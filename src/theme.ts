const themeConfig = {
  ms: {
    SM: 576, // Small devices (landscape phones, 576px and up)
    MD: 767, // Medium devices (tablets, 768px and up)
    LG: 992, // Large devices (desktops, 992px and up)
    XL: 1200, // X-Large devices (large desktops, 1200px and up)
  },
  colors: {
    'neutral-100': '#FAFAFA',
    'neutral-200': '#F0F0F2',
    'neutral-300': '#D6D6DB',
    'neutral-600': '#9F9FAC',
    'neutral-700': '#6B6B7B',
    'neutral-800': '#535360',
    'secondary-300': '#E9F0F7',
    'secondary-400': '#BCD1E7',
    'secondary-500': '#62AAF2',
    'secondary-800': '#0E59A5',
    'primary-300': '#EDEDFC',
    'primary-500': '#918AE5',
    'primary-900': '#090066',
    'alerts-hint-700': '#9B5A0E',
    'message-warning-200': '#FCEEDE',
    'message-warning-500': '#EC9024',
    white: '#FFFFFF',
    black: '#252527',
    push: '#00A300',
    remove: '#FF3399',
  },
  typography: {
    'base/sm':
      'font-weight:400;font-size:14px;line-height:19.32px;font-family:"IBM Plex Sans", sans-serif;',
    'base/base':
      'font-weight:400;font-size:16px;line-height:22.08px;font-family:"IBM Plex Sans", sans-serif;',
    'base/base bold':
      'font-weight:700;font-size:16px;line-height:22.08px;font-family:"IBM Plex Sans", sans-serif;',
    'base/md':
      'font-weight:400;font-size:19px;line-height:26.22px;font-family:"IBM Plex Sans", sans-serif;',
    'base/md bold':
      'font-weight:700;font-size:19px;line-height:26.22px;font-family:"IBM Plex Sans", sans-serif;',
    'base/sm bold':
      'font-weight:700;font-size:14px;line-height:26.22px;font-family:"IBM Plex Sans", sans-serif;',
    'display/xs bold':
      'font-weight:700;font-size:12px;line-height:16.56px;font-family:"IBM Plex Mono", monospace;',
    'display/xs regular':
      'font-weight:400;font-size:12px;line-height:16.56px;font-family:"IBM Plex Mono", monospace;',
    'display/sm regular':
      'font-weight:400;font-size:14px;line-height:19.32px;font-family:"IBM Plex Mono", monospace;',
    'display/sm bold':
      'font-weight:700;font-size:14px;line-height:19.32px;font-family:"IBM Plex Mono", monospace;',
    'display/base':
      'font-weight:500;font-size:17px;line-height:23.46px;font-family:"IBM Plex Mono", monospace;',
    'display/base bold':
      'font-weight:700;font-size:17px;line-height:23.46px;font-family:"IBM Plex Mono", monospace;',
    'display/md bold':
      'font-weight:700;font-size:19px;line-height:26.22px;font-family:"IBM Plex Mono", monospace;',
    'display/xl bold':
      'font-weight:700;font-size:24px;line-height:33.12px;font-family:"IBM Plex Mono", monospace;',
    'display/md medium':
      'font-weight:500;font-size:19px;line-height:26.22px;font-family:"IBM Plex Mono", monospace;',
    'rc/base':
      'font-weight:500;font-size:16px;line-height:22.5px;font-family:"Roboto Condensed", sans-serif;',
  },
  sizing: {
    '1': '4px',
    '1.5': '6px',
    '2.5': '10px',
    '3': '12px',
    '3.5': '14px',
    '5': '20px',
    '7': '28px',
    '8': '32px',
    '10': '40px',
    '15': '60px',
    '16': '64px',
  },
}

type TyKey = keyof typeof themeConfig.typography

const theme = {
  ...themeConfig,
  ty: (mobile: TyKey, desktop?: TyKey) =>
    desktop
      ? `${theme.typography[mobile]} @media (min-width: ${themeConfig.ms.MD}px) {${theme.typography[desktop]}}`
      : theme.typography[mobile],
}

export default theme
