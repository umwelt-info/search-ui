import { useRef, useEffect, useCallback } from 'react'

import L from 'leaflet'
import 'leaflet-draw'
import 'leaflet/dist/leaflet.css'
import 'leaflet-draw/dist/leaflet.draw.css'

import theme from './theme'
import { BoundingBox } from './api'
import { setBoundingBox, triggerSearch, useStore } from './store'

export function RegionDetailsMap(boundingBoxes: BoundingBox[]) {
  const mapRef = useRef<L.DrawMap | null>(null)
  const featureGroupRef = useRef<L.FeatureGroup | null>(null)

  /**
   * init leaflet
   */
  useEffect(() => {
    if (mapRef.current) return
    const { map, featureGroup } = createMap({ mapId: 'leaflet-detail-map' })

    featureGroupRef.current = featureGroup
    mapRef.current = map
  }, [])

  /** set center */
  useEffect(() => {
    if (!mapRef.current) return

    const { centerLat, centerLon, zoom } = calcStartingView(boundingBoxes)
    mapRef.current.setView([centerLat, centerLon], zoom)
  }, [boundingBoxes])

  /** draw marker or rectangle */
  useEffect(() => {
    if (!featureGroupRef.current) return

    featureGroupRef.current.clearLayers()

    for (const boundingBox of boundingBoxes) {
      const layer = createLayer(boundingBox)
      featureGroupRef.current.addLayer(layer)
    }
  }, [boundingBoxes])

  return 'leaflet-detail-map'
}

export function RegionFilterMap(
  setMaximized: (newState: React.SetStateAction<boolean>) => void,
) {
  const mapRef = useRef<L.DrawMap | null>(null)
  const featureGroupRef = useRef<L.FeatureGroup | null>(null)

  const boundingBox = useStore((store) => store.boundingBox)

  const updateMapSize = useCallback(
    (map: L.DrawMap, maximized: React.SetStateAction<boolean>) => {
      setMaximized(maximized)
      setTimeout(() => map.invalidateSize())
    },
    [setMaximized],
  )

  /**
   * init leaflet
   */
  useEffect(() => {
    if (mapRef.current) return
    const { map, featureGroup } = createMap({
      mapId: 'leaflet-region-filter',
      initialView: { centerLat: 51.1657, centerLon: 9.9167, zoom: 5 },
    })

    mapRef.current = map
    featureGroupRef.current = featureGroup

    const MaximizeToggle = L.Control.extend({
      onAdd: function () {
        const button = L.DomUtil.create('div', 'leaflet-maximize')

        L.DomEvent.on(button, 'click', () => {
          updateMapSize(map, (maximized: boolean) => !maximized)
        })

        window.addEventListener('keydown', (e) => {
          if (e.key === 'Escape') {
            updateMapSize(map, false)
          }
        })

        return button
      },
    })

    map.addControl(
      new MaximizeToggle({
        position: 'topright',
      }),
    )
  })

  /**
   * when region-filter is set we show edit and remove buttons
   * also we show a rectangle with the regionFilter value
   */
  useEffect(() => {
    if (!mapRef.current) return
    if (!featureGroupRef.current) return
    if (!boundingBox) return

    const map = mapRef.current
    const featureGroup = featureGroupRef.current

    const rectangle = createRectangle(boundingBox)

    const drawControl = createDrawControl(featureGroupRef.current, {
      rectangle: false,
      edit: true,
      remove: true,
    })
    const onDrawDelete: L.LeafletEventHandlerFn = () => {
      setBoundingBox(null)
      triggerSearch()
      updateMapSize(map, false)
    }
    const onEditEnd: L.LeafletEventHandlerFn = () => {
      const bounds = rectangle.getBounds()
      setBoundingBox([
        bounds.getSouth(),
        bounds.getWest(),
        bounds.getNorth(),
        bounds.getEast(),
      ])
      triggerSearch()
      updateMapSize(map, false)
    }

    featureGroup.addLayer(rectangle)
    map.addControl(drawControl)
    map.on(L.Draw.Event.DELETESTART, onDrawDelete)
    map.on(L.Draw.Event.EDITED, onEditEnd)

    return () => {
      map.removeLayer(rectangle)
      map.removeControl(drawControl)
      map.off(L.Draw.Event.DELETESTART, onDrawDelete)
      map.off(L.Draw.Event.EDITED, onEditEnd)
    }
  }, [boundingBox, updateMapSize])

  /**
   * when region filter is not set we show rectangle button
   */
  useEffect(() => {
    if (!mapRef.current) return
    if (!featureGroupRef.current) return
    if (boundingBox) return

    const map = mapRef.current
    const drawControl = createDrawControl(featureGroupRef.current, {
      rectangle: true,
      edit: false,
      remove: false,
    })
    const onDrawCreate: L.LeafletEventHandlerFn = (e) => {
      const bounds = e.layer.getBounds()
      setBoundingBox([
        bounds.getSouth(),
        bounds.getWest(),
        bounds.getNorth(),
        bounds.getEast(),
      ])
      triggerSearch()
      updateMapSize(map, false)
    }
    map.addControl(drawControl)
    map.on(L.Draw.Event.CREATED, onDrawCreate)

    return () => {
      map.removeControl(drawControl)
      map.off(L.Draw.Event.CREATED, onDrawCreate)
    }
  }, [boundingBox, updateMapSize])

  return 'leaflet-region-filter'
}

function calcStartingView(boundingBoxes: BoundingBox[]) {
  let maxLat = -90
  let minLat = 90
  let maxLon = -180
  let minLon = 180

  for (const boundingBox of boundingBoxes) {
    maxLat = Math.max(maxLat, boundingBox.max.y)
    minLat = Math.min(minLat, boundingBox.min.y)
    maxLon = Math.max(maxLon, boundingBox.max.x)
    minLon = Math.min(minLon, boundingBox.min.x)
  }

  const centerLat = (maxLat + minLat) / 2
  const centerLon = (maxLon + minLon) / 2

  const zoom = Math.min(
    10,
    Math.max(1, Math.floor(Math.log2(360 / (maxLon - minLon)))),
  )

  return { centerLat, centerLon, zoom }
}

function createLayer(boundingBox: BoundingBox) {
  const min = boundingBox.min
  const max = boundingBox.max

  if (min.x === max.x && min.y === max.y) {
    return L.marker([min.y, min.x], { icon: L.divIcon({ html: PIN }) })
  } else {
    return L.rectangle(
      [
        [min.y, min.x],
        [max.y, max.x],
      ],
      {
        stroke: true,
        color: theme.colors['primary-900'],
        weight: 3,
        opacity: 0.7,
        fill: true,
        fillOpacity: 0.3,
      },
    )
  }
}

function createRectangle(boundingBox: [number, number, number, number]) {
  return L.rectangle(
    [
      [boundingBox[0], boundingBox[1]],
      [boundingBox[2], boundingBox[3]],
    ],
    {
      stroke: true,
      color: theme.colors.push,
      weight: 4,
      opacity: 0.5,
      fill: true,
      fillOpacity: 0.2,
    },
  )
}

function createDrawControl(
  featureGroup: L.FeatureGroup,
  options: {
    rectangle: boolean
    edit: boolean
    remove: boolean
  },
) {
  return new L.Control.Draw({
    position: 'bottomright',
    draw: {
      circle: false,
      circlemarker: false,
      marker: false,
      polyline: false,
      polygon: false,
      rectangle: options.rectangle
        ? { shapeOptions: { color: theme.colors.push } }
        : false,
    },
    edit: {
      featureGroup: featureGroup,
      edit: options.edit ? {} : false,
      remove: options.remove,
    },
  })
}

type CreateMapArgs = {
  mapId: string
  initialView?: { centerLat: number; centerLon: number; zoom: number }
}

function createMap(args: CreateMapArgs) {
  const map = L.map(args.mapId, {})

  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    crossOrigin: true,
    attribution:
      '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  }).addTo(map)

  if (args.initialView) {
    map.setView(
      [args.initialView.centerLat, args.initialView.centerLon],
      args.initialView.zoom,
    )
  }

  const featureGroup = new L.FeatureGroup()
  map.addLayer(featureGroup)

  L.drawLocal.draw.toolbar.buttons.rectangle = 'Zeichne ein Rechteck'
  L.drawLocal.draw.handlers.rectangle.tooltip.start =
    'Klicken und ziehen, um ein Rechteck zu zeichnen'
  L.drawLocal.draw.toolbar.actions.text = 'Abbrechen'
  L.drawLocal.draw.handlers.simpleshape.tooltip.end =
    'Maus loslassen, um das Zeichnen zu beenden'
  L.drawLocal.edit.toolbar.actions.save.title = 'Änderungen speichern'
  L.drawLocal.edit.toolbar.actions.save.text = 'Speichern'
  L.drawLocal.edit.toolbar.actions.cancel.text = 'Abbrechen'
  L.drawLocal.edit.toolbar.actions.cancel.title = 'Alle Änderungen verwerfen'
  L.drawLocal.edit.toolbar.buttons.edit = 'Bearbeiten'
  L.drawLocal.edit.toolbar.buttons.remove = 'Löschen'
  L.drawLocal.edit.handlers.edit.tooltip.text = 'Bearbeiten Sie das Rechteck'
  L.drawLocal.edit.handlers.edit.tooltip.subtext =
    '"Speichern", um die Änderungen zu übernehmen'

  return { map, featureGroup }
}

// Workaround for <https://github.com/Leaflet/Leaflet.draw/issues/1026>
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.type = ''

const PIN = `
<svg stroke="currentColor" fill="currentColor" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" xmlns="http://www.w3.org/2000/svg">
    <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
    <circle cx="12" cy="10" r="3" fill="none"></circle>
</svg>
`
