export function computePaginationOptions(
  page: number,
  pages: number,
): number[] {
  const options = []

  if (page + 2 < pages && pages > 5) {
    const start = Math.max(page - 2, 1)
    for (let option = start; option < start + 5; option++) {
      options.push(option)
    }
  } else {
    const start = Math.max(pages - 4, 1)
    for (let option = start; option <= pages; option++) {
      options.push(option)
    }
  }

  return options
}

export function clearFocus() {
  const element = document.activeElement
  if (element && 'blur' in element && typeof element.blur === 'function') {
    element.blur()
  }
}
