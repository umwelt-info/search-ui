import styled, { css } from 'styled-components'
import { useMemo, useState, useRef, useEffect } from 'react'
import { FiSearch, FiX } from 'react-icons/fi'

import theme from '../theme'
import {
  useStore,
  setQuery,
  AnyRoot,
  setAnyRoot,
  triggerSearch,
  triggerComplete,
  triggerDetail,
  setPage,
  setDetail,
} from '../store'
import { writeQuery, MATCHING_FIELD_LABELS } from '../parser'
import { Clause, CompleteResponse } from '../api'
import { MAX_COMPLETIONS, MAX_REPLACEMENTS } from '../config'
import { ROOT_LABELS } from '../labels'
import { clearFocus } from '../utils'

type Props = {
  homepage?: boolean
}

export default function Query(props: Props) {
  const query = useStore((store) => store.query)
  const searchedQuery = useStore((store) => store.searchedQuery)
  const completedQuery = useStore((store) => store.completedQuery)
  const completions = useStore((store) => store.completions)
  const similarTerms = useStore((store) => store.results.similar_terms)
  const relatedTerms = useStore((store) => store.results.related_terms)

  const options = useMemo(
    () => computeOptions(completedQuery, completions),
    [completedQuery, completions],
  )

  const hasSimilarTerms = similarTerms.length !== 0
  const hasRelatedTerms = relatedTerms.length !== 0
  const hasReplacementTerms = hasSimilarTerms || hasRelatedTerms
  let replacementTermsLabel

  if (hasSimilarTerms && hasRelatedTerms) {
    replacementTermsLabel = 'Ähnliche & verwandte'
  } else if (hasRelatedTerms) {
    replacementTermsLabel = 'Verwandte'
  } else {
    replacementTermsLabel = 'Ähnliche'
  }

  const [allReplacementTerms, setAllReplacementTerms] = useState(false)

  const [replacementTerms, moreReplacementTerms] = useMemo(() => {
    let replacementTerms = similarTerms.concat(
      relatedTerms.flatMap(([term, relatedTerms]) =>
        relatedTerms.map((relatedTerm): [string, string] => [
          term,
          relatedTerm,
        ]),
      ),
    )

    const moreReplacementTerms = replacementTerms.length > MAX_REPLACEMENTS

    replacementTerms = allReplacementTerms
      ? replacementTerms
      : replacementTerms.slice(0, MAX_REPLACEMENTS)

    return [replacementTerms, moreReplacementTerms]
  }, [similarTerms, relatedTerms, allReplacementTerms])

  const [focused, containerRef] = useFocused()
  const [active, setActive] = useState<number | null>(null)

  useEffect(() => {
    if (!focused || !options) return setActive(null)

    const onKeyDown = (event: KeyboardEvent) => {
      if (event.key === 'ArrowDown') {
        setActive((prev) => {
          if (prev === null) return 0
          if (prev >= options.length - 1) return prev
          return prev + 1
        })
        // prevent scroll down
        event.preventDefault()
      } else if (event.key === 'ArrowUp') {
        setActive((prev) => {
          if (prev === null || prev === 0) return null
          return prev - 1
        })
        // prevent scroll up
        event.preventDefault()
      } else if (event.key === 'Escape') {
        clearFocus()
      } else if (event.key === 'Enter' && active !== null) {
        onClickOption(options[active], props.homepage)
        event.preventDefault()
      }
    }

    window.addEventListener('keydown', onKeyDown)
    return () => window.removeEventListener('keydown', onKeyDown)
  }, [props.homepage, options, focused, active])

  return (
    <>
      <QueryWrapper ref={containerRef}>
        <QueryContainer>
          <QueryInput
            value={query}
            onChange={onChange}
            aria-label="Suche"
            placeholder="Ich suche ... "
          />
          <ClearButton
            title="Anfrage zurücksetzen"
            type="reset"
            $show={query.length !== 0}
            onClick={() => onClear(props.homepage)}
          >
            <FiX />
          </ClearButton>
          <SearchButton
            type="submit"
            title="Suchen"
            $active={query.length !== 0}
          >
            <FiSearch />
          </SearchButton>
        </QueryContainer>

        {focused && options.length !== 0 && (
          <OptionsContainer className="options">
            {options.map((option, index) => (
              <button
                key={`option-${index}`}
                type="button"
                title={option.tooltip}
                onClick={() => onClickOption(option, props.homepage)}
                className={index == active ? 'active' : undefined}
              >
                {option.label}
                {option.context && (
                  <span className="context">{option.context}</span>
                )}
              </button>
            ))}
          </OptionsContainer>
        )}
      </QueryWrapper>

      {!focused && hasReplacementTerms && (
        <ReplacementTerms>
          <p>{replacementTermsLabel} Begriffe:</p>
          {replacementTerms.map(([term, replacement]) => (
            <button
              type="button"
              key={replacement}
              onClick={() =>
                onClickReplacementTerm(
                  searchedQuery,
                  term,
                  replacement,
                  props.homepage,
                )
              }
            >
              {replacement}
            </button>
          ))}
          {moreReplacementTerms && (
            <button
              type="button"
              onClick={() => setAllReplacementTerms(!allReplacementTerms)}
              className="read-more-button"
            >
              {allReplacementTerms ? 'Weniger anzeigen' : 'Mehr anzeigen'}
            </button>
          )}
        </ReplacementTerms>
      )}
    </>
  )
}

function onChange(event: React.ChangeEvent<HTMLInputElement>) {
  setQuery(event.target.value)

  triggerComplete()
}

function onClear(homepage?: boolean) {
  setQuery('')
  setPage(1)

  triggerSearch(homepage)
  triggerComplete()
}

function onClickOption(option: Option, homepage?: boolean) {
  setQuery(option.query)

  if (option.root) {
    const [key, value] = option.root

    setAnyRoot(key, value)
  }

  setPage(1)
  setDetail(null)

  triggerSearch(homepage)
  triggerComplete()
  triggerDetail()

  clearFocus()
}

function onClickReplacementTerm(
  searchedQuery: Clause[],
  term: string,
  replacement: string,
  homepage?: boolean,
) {
  const query = searchedQuery.map((clause) => Object.create(clause))

  for (const clause of query) {
    if (clause.phrase === term) {
      clause.phrase = replacement.toLowerCase()
    }
  }

  setQuery(writeQuery(query))

  setPage(1)
  setDetail(null)

  triggerSearch(homepage)
  triggerComplete()
  triggerDetail()
}

type Option = {
  label: string
  tooltip: string | undefined
  context: string | null
  query: string
  root: [AnyRoot, string] | null
}

function computeOptions(
  completedQuery: Clause[],
  completions: CompleteResponse,
): Option[] {
  const query = completedQuery.map((clause) => Object.create(clause))

  const lastClause = query.at(-1)
  if (!lastClause) {
    return []
  }

  type RawOption = {
    priority: number
    count: number
    target: 'completion' | 'replacement' | 'matching_field' | AnyRoot
    value: string
    uniqueSuffix?: number
  }

  const rootRawOptions = (target: AnyRoot) => {
    return ([value, count]: [string, number]) => ({
      priority: 0,
      count,
      target,
      value,
    })
  }

  const tags = new Set<string>()

  const isFirstTag = (topic: string): boolean => {
    const tag = topic.split('/').at(-1)!

    if (tags.has(tag)) {
      return false
    }

    tags.add(tag)
    return true
  }

  const options = completions.similar_terms
    .map(
      ([value, count]): RawOption => ({
        priority: 1,
        count,
        target: 'replacement',
        value,
      }),
    )
    .concat(
      completions.matching_fields.map(([value, count]) => ({
        priority: 2,
        count,
        target: 'matching_field',
        value,
      })),
      completions.completions.map(([value, count]) => ({
        priority: 0,
        count,
        target: 'completion',
        value,
      })),
      completions.types.map(rootRawOptions('typesRoot')),
      completions.topics.map(rootRawOptions('topicsRoot')),
      completions.origins.map(rootRawOptions('originsRoot')),
      completions.licenses.map(rootRawOptions('licensesRoot')),
      completions.languages.map(rootRawOptions('languagesRoot')),
      completions.resource_types.map(rootRawOptions('resourceTypesRoot')),
    )
    .sort((lhs, rhs) => {
      const priority = rhs.priority - lhs.priority
      if (priority !== 0) {
        return priority
      }

      return rhs.count - lhs.count
    })
    .filter(
      (option) => option.target !== 'topicsRoot' || isFirstTag(option.value),
    )
    .slice(0, MAX_COMPLETIONS)

  for (const option of options) {
    if (
      option.target === 'completion' ||
      option.target === 'replacement' ||
      option.target === 'matching_field'
    ) {
      continue
    }

    const components = option.value.split('/').slice(1)

    let uniqueSuffix = 1

    for (const otherOption of options) {
      if (otherOption === option || otherOption.target !== option.target) {
        continue
      }

      const otherComponents = otherOption.value.split('/').slice(1)

      while (
        components.slice(-uniqueSuffix).join('\\') ===
        otherComponents.slice(-uniqueSuffix).join('\\')
      ) {
        uniqueSuffix++
      }
    }

    option.uniqueSuffix = uniqueSuffix
  }

  const lastPhrase = lastClause.phrase
  const queryWithoutLastClause = writeQuery(query.slice(0, -1))

  return options.map((option) => {
    switch (option.target) {
      case 'completion':
        lastClause.phrase = lastPhrase + option.value

        return {
          label: lastClause.phrase,
          tooltip: undefined,
          context: null,
          query: writeQuery(query),
          root: null,
        }
      case 'replacement':
        lastClause.phrase = option.value.toLowerCase()

        return {
          label: lastClause.phrase,
          tooltip: undefined,
          context: ' ähnlicher Begriff',
          query: writeQuery(query),
          root: null,
        }
      case 'matching_field': {
        lastClause.field_name = option.value

        const fieldName = MATCHING_FIELD_LABELS.get(option.value)

        return {
          label: lastClause.phrase,
          tooltip: undefined,
          context: ` Übereinstimmung in ${fieldName}`,
          query: writeQuery(query),
          root: null,
        }
      }
      default: {
        const components = option.value.split('/').slice(1)

        return {
          label: components.slice(-option.uniqueSuffix!).join('\\'),
          tooltip: components.join('\\'),
          context: ` in ${ROOT_LABELS[option.target]}`,
          query: queryWithoutLastClause,
          root: [option.target, option.value] as [AnyRoot, string],
        }
      }
    }
  })
}

function useFocused() {
  const [focused, setFocused] = useState(false)
  const containerRef = useRef<HTMLDivElement | null>(null)

  useEffect(() => {
    const element = containerRef.current
    if (!element) {
      return
    }

    const onFocusIn = () => setFocused(true)

    const onFocusOut = (event: FocusEvent) => {
      if (
        !event.relatedTarget ||
        !element.contains(event.relatedTarget as Node)
      ) {
        setFocused(false)
      }
    }

    element.addEventListener('focusin', onFocusIn)
    element.addEventListener('focusout', onFocusOut)

    return () => {
      element.removeEventListener('focusin', onFocusIn)
      element.removeEventListener('focusout', onFocusOut)
    }
  }, [])

  return [focused, containerRef] as const
}

const QueryWrapper = styled.div`
  display: flex;
  flex-direction: column;
  grid-area: query;
  min-width: 0;
`

const QueryContainer = styled.div`
  display: flex;
  justify-content: space-between;
  border: 1px solid ${theme.colors['neutral-700']};
  background: ${theme.colors.white};
  gap: ${theme.sizing['5']};
  > button {
    border: none;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    padding: 0 ${theme.sizing['5']};
    > svg {
      scale: 2;
      min-width: 11px;
    }
  }
`

const QueryInput = styled.input`
  flex: 1 1 0%;
  ${theme.ty('base/sm bold', 'base/md bold')}
  color: ${theme.colors.black};
  padding: ${theme.sizing['5']};
  border: none;
  background: none;
  width: 0;
  &:placeholder {
    color:  ${theme.colors['neutral-700']};
  }
  &:active,
  &:focus {
    outline: none;
`

const ClearButton = styled.button<{ $show: boolean }>`
  display: ${({ $show }) => ($show ? 'flex' : 'none')};
  background: none;
  > svg {
    stroke: ${theme.colors.black};
  }
`

const SearchButton = styled.button<{ $active: boolean }>`
  display: flex;
  background: ${({ $active }) =>
    $active
      ? css`
          ${theme.colors['secondary-800']}
        `
      : 'none'};
  > svg {
    stroke: ${({ $active }) =>
      $active
        ? css`
            ${theme.colors.white}
          `
        : css`
            ${theme.colors.black}
          `};
  }
`

const OptionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${theme.colors['neutral-700']};
  border-top: 0;
  padding: 0;
  background: ${theme.colors.white};
  margin: 0;
  box-sizing: border-box;
  width: 100%;

  > button {
    ${theme.ty('base/base', 'base/md bold')}
    border: none;
    background: none;
    padding: 0;
    cursor: pointer;
    text-align: left;
    padding: ${theme.sizing['2.5']} ${theme.sizing['5']};

    &:hover,
    &.active {
      background: ${theme.colors['primary-300']};
    }

    > .context {
      ${theme.ty('base/base')}
      color: ${theme.colors['neutral-700']};
      margin-left: ${theme.sizing['2.5']};
    }
  }
`

const ReplacementTerms = styled.div`
  grid-area: replacement-terms;
  display: flex;
  flex-wrap: wrap;
  ${theme.ty('base/base')};
  padding-left: ${theme.sizing['5']};
  gap: ${theme.sizing['1.5']};

  p {
    margin: 0;
  }

  button {
    background: 0;
    border: none;
    padding: 0;
    cursor: pointer;
    text-decoration: underline;
    color: ${theme.colors['primary-900']};
    ${theme.ty('base/base bold')};
  }

  .read-more-button {
    ${theme.ty('base/sm', 'base/base')};
  }
`
