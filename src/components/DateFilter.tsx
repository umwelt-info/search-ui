import styled from 'styled-components'
import { useRef } from 'react'
import { FiArrowRight, FiCalendar } from 'react-icons/fi'

import {
  triggerSearch,
  useStore,
  setTimeRangeFrom,
  setTimeRangeUntil,
} from '../store'
import theme from '../theme'

export default function DateFilter() {
  return (
    <DateFilterWrapper className="DateFilter" title="Zeitraum">
      <DateInput
        value={useStore((store) => store.timeRangeFrom ?? '')}
        onChange={setTimeRangeFrom}
        title="Zeitraum von"
      />
      <div className="divider">
        <FiArrowRight />
      </div>
      <DateInput
        value={useStore((store) => store.timeRangeUntil ?? '')}
        onChange={setTimeRangeUntil}
        title="Zeitraum bis"
      />
    </DateFilterWrapper>
  )
}

type DateInputProps = {
  value: string | undefined
  onChange: (value: string | null) => void
  title: string
}

function DateInput(props: DateInputProps) {
  const inputRef = useRef<HTMLInputElement>(null)

  const handleDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.onChange(event.target.value || null)

    triggerSearch()
  }

  const handleIconClick = () => {
    inputRef.current?.showPicker()
  }

  return (
    <DateInputWrapper>
      <input
        type="date"
        aria-label={props.title}
        ref={inputRef}
        value={props.value}
        onChange={handleDateChange}
      />
      <button
        onClick={handleIconClick}
        title={`${props.title} im Kalender auswählen`}
      >
        <FiCalendar color={theme.colors['neutral-700']} />
      </button>
    </DateInputWrapper>
  )
}

const DateFilterWrapper = styled.div`
  display: flex;
  border: 1px solid ${theme.colors['black']};

  > .divider {
    width: 40px;
    border-left: 1px solid ${theme.colors['black']};
    border-right: 1px solid ${theme.colors['black']};
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 20px;
    color: ${theme.colors['neutral-700']};
  }
`

const DateInputWrapper = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  padding-left: ${theme.sizing['2.5']};
  gap: ${theme.sizing['2.5']};
  background: ${theme.colors['white']};
  cursor: pointer;
  position: relative;

  > input {
    ${theme.ty('display/sm regular')}
    padding: ${theme.sizing['2.5']} 0;
    color: ${theme.colors['black']};
    flex: 1;
    border: none;

    &:focus {
      outline: none;
    }
  }

  > button {
    padding: calc(${theme.sizing['2.5']} - 1px);
    background: ${theme.colors['white']};
    border: none;
    cursor: pointer;
    position: absolute;
    right: 0;
    top: 0;

    font: initial; // reset global styling

    /** resize icon and place it in the optical center */
    > svg {
      transform: scale(1.8);
      margin-bottom: -5px;
    }
    /** firefox behaves a bit different */
    @-moz-document url-prefix() {
      > svg {
        margin-bottom: -3px;
      }
    }
  }
`
