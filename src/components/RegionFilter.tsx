import styled, { css } from 'styled-components'
import { useState, useEffect } from 'react'

import { RegionFilterMap } from '../leaflet'

export default function RegionFilter() {
  const [maximized, setMaximized] = useState(false)
  const elId = RegionFilterMap(setMaximized)

  useEffect(() => {
    const createSvg = (
      el: Element | null,
      svg: string,
      rewrite: boolean = false,
    ) => {
      if (!el) {
        return
      }

      const svgWrapper = el.querySelector('.svg-wrapper')

      if (!svgWrapper) {
        const svgWrapper = document.createElement('div')
        svgWrapper.classList.add('svg-wrapper')
        svgWrapper.innerHTML = svg
        el.appendChild(svgWrapper)
      } else if (rewrite) {
        svgWrapper.innerHTML = svg
      }
    }

    createSvg(document.querySelector('.leaflet-draw-draw-rectangle'), RECT)
    createSvg(document.querySelector('.leaflet-draw-edit-edit'), EDIT)
    createSvg(document.querySelector('.leaflet-draw-edit-remove'), REMOVE)
    createSvg(
      document.querySelector('.leaflet-maximize'),
      maximized ? MINIMIZE : MAXIMIZE,
      true,
    )
  })

  return (
    <>
      <h2>Region</h2>
      <MapContainer
        className="RegionFilter"
        title="Region"
        $maximized={maximized}
      >
        <div id={elId} />
      </MapContainer>
    </>
  )
}

const MapContainer = styled.div<{ $maximized: boolean }>`
  height: 350px;

  > #leaflet-region-filter {
    height: 100%;
    width: 100%;
  }

  ${({ $maximized }) =>
    $maximized &&
    css`
      position: fixed;
      top: 0;
      left: 0;
      height: 100%;
      width: 100%;
      z-index: 400;

      > #leaflet-region-filter {
        top: 10vh;
        left: 10vw;
        height: 80vh;
        width: 80vw;
        box-shadow: 0 0 0 10000vmax rgba(20, 17, 52, 0.95);
      }
    `}

  && {
    .leaflet-draw-draw-rectangle {
      background-color: #fff;
      background-image: none;
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 3px;
      &:hover {
        background-color: whitesmoke;
      }
      > .svg-wrapper {
        display: flex;
      }
    }

    .leaflet-draw-edit-edit {
      background-color: #fff;
      background-image: none;
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 3px;
      &:hover {
        background-color: whitesmoke;
      }
      > .svg-wrapper {
        display: flex;
      }
    }

    .leaflet-draw-edit-remove {
      background-color: #fff;
      background-image: none;
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 3px;
      &:hover {
        background-color: whitesmoke;
      }
      > .svg-wrapper {
        display: flex;
      }
    }

    .leaflet-maximize {
      background-color: #fff;
      background-image: none;
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 6px;
      border: 2px solid rgba(0, 0, 0, 0.2);
      border-radius: 4px;
      background-clip: padding-box;
      cursor: pointer;
      &:hover {
        background-color: whitesmoke;
      }
      > .svg-wrapper {
        display: flex;
      }
    }
  }
`
const RECT = `
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <g clip-path="url(#clip0_930_1986)">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M20 4H4V20H20V4ZM2 2V22H22V2H2Z" fill="#535360" />
    <path
      d="M0 3C0 1.34315 1.34315 0 3 0C4.65685 0 6 1.34315 6 3C6 4.65685 4.65685 6 3 6C1.34315 6 0 4.65685 0 3Z"
      fill="#535360" />
    <path
      d="M18 3C18 1.34315 19.3431 0 21 0C22.6569 0 24 1.34315 24 3C24 4.65685 22.6569 6 21 6C19.3431 6 18 4.65685 18 3Z"
      fill="#535360" />
    <path
      d="M18 21C18 19.3431 19.3431 18 21 18C22.6569 18 24 19.3431 24 21C24 22.6569 22.6569 24 21 24C19.3431 24 18 22.6569 18 21Z"
      fill="#535360" />
    <path
      d="M0 21C0 19.3431 1.34315 18 3 18C4.65685 18 6 19.3431 6 21C6 22.6569 4.65685 24 3 24C1.34315 24 0 22.6569 0 21Z"
      fill="#535360" />
  </g>
  <defs>
    <clipPath id="clip0_930_1986">
      <rect width="24" height="24" fill="white" />
    </clipPath>
  </defs>
</svg>
`

const EDIT = `
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path
    d="M10.9395 4.06077H3.93945C3.40902 4.06077 2.90031 4.27149 2.52524 4.64656C2.15017 5.02163 1.93945 5.53034 1.93945 6.06077V20.0608C1.93945 20.5912 2.15017 21.0999 2.52524 21.475C2.90031 21.8501 3.40902 22.0608 3.93945 22.0608H17.9395C18.4699 22.0608 18.9786 21.8501 19.3537 21.475C19.7287 21.0999 19.9395 20.5912 19.9395 20.0608V13.0608M18.4395 2.56077C18.8373 2.16295 19.3768 1.93945 19.9395 1.93945C20.5021 1.93945 21.0416 2.16295 21.4395 2.56077C21.8373 2.9586 22.0608 3.49816 22.0608 4.06077C22.0608 4.62338 21.8373 5.16295 21.4395 5.56077L11.9395 15.0608L7.93945 16.0608L8.93945 12.0608L18.4395 2.56077Z"
    stroke="#535360" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
</svg>
`

const REMOVE = `
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
`

const MAXIMIZE = `
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-maximize-2"><polyline points="15 3 21 3 21 9"></polyline><polyline points="9 21 3 21 3 15"></polyline><line x1="21" y1="3" x2="14" y2="10"></line><line x1="3" y1="21" x2="10" y2="14"></line></svg>
`

const MINIMIZE = `
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-minimize-2"><polyline points="4 14 10 14 10 20"></polyline><polyline points="20 10 14 10 14 4"></polyline><line x1="14" y1="10" x2="21" y2="3"></line><line x1="3" y1="21" x2="10" y2="14"></line></svg>
`
