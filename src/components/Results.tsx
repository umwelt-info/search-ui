import styled, { css } from 'styled-components'
import { useState, useCallback } from 'react'
import {
  FiExternalLink,
  FiCornerDownRight,
  FiFilter,
  FiHelpCircle,
  FiCheck,
  FiCopy,
  FiShare2,
  FiAlertTriangle,
} from 'react-icons/fi'
import { TbApi } from 'react-icons/tb'

import theme from '../theme'
import { buildTypeLabel } from '../labels'
import {
  useStore,
  setDetail,
  triggerDetail,
  toggleMobileFilterDrawer,
} from '../store'
import { Dataset, Type } from '../api'

export default function Results() {
  const results = useStore((store) => store.results.results)
  const count = useStore((store) => store.results.count)

  return (
    <ResultContainer>
      <ResultContext>
        <div id="result-count">
          {count === undefined
            ? 'Ergebnisse werden geladen ...'
            : count < 1
              ? 'Keine Ergebnisse'
              : count === 1
                ? '1 Ergebnis'
                : `${count.toLocaleString()} Ergebnisse`}
        </div>
        <div className="spacer"></div>
        <a
          className="external-link"
          href="/artikel/ueber-die-suche-bei-umweltinfo"
          title="Über die Suche"
        >
          <FiHelpCircle size={18} />
        </a>
        <a
          className="external-link"
          href="https://md.umwelt.info/swagger-ui/"
          title="API-Dokumentation anzeigen"
        >
          <TbApi size={22} />
        </a>
        <ShareButton></ShareButton>
        <button
          className="show-filter"
          onClick={toggleMobileFilterDrawer}
          title="Filter anzeigen"
        >
          <FiFilter size={18} />
        </button>
      </ResultContext>
      <ResultsList id="results">
        {count !== undefined && count < 1 && (
          <div className="explainer">
            <p>
              Versuchen Sie es mit einem anderen Suchbegriff oder anderen
              Filtereinstellungen.
            </p>
            <p>
              <a href="/artikel/ueber-die-suche-bei-umweltinfo">
                Alle Suchfunktionen im Überblick.
              </a>
            </p>
          </div>
        )}
        {results.map((dataset, index) => (
          <Result key={`result-${index}`} dataset={dataset} />
        ))}
      </ResultsList>
    </ResultContainer>
  )
}

function Result({ dataset }: { dataset: Dataset }) {
  const detail = dataset.source + '/' + dataset.id

  const types =
    dataset.types && dataset.types.length
      ? buildTypeLabel(dataset.types[0])
      : null

  const tags = (dataset.tags ?? [])
    .slice(0, 5)
    .map((tag) => {
      if ('Other' in tag) {
        return tag['Other']
      } else if ('Umthes' in tag) {
        return tag['Umthes'].label
      } else {
        console.error('unknown tag', tag)
        return null
      }
    })
    .filter(Boolean)
    .join(', ')

  const origins = (dataset.origins ?? [])
    .map((origin) => origin.slice(1).split('/').join('\\'))
    .join(', ')

  const onClick = useCallback(
    (event: React.MouseEvent<HTMLAnchorElement>) => {
      event.preventDefault()

      setDetail(detail)

      triggerDetail()

      // Scroll to a bit above the results
      const results = document.getElementById('results')
      window.scrollTo({ top: results!.offsetTop - 100, behavior: 'smooth' })
    },
    [detail],
  )

  return (
    <ResultWrapper>
      <div className="container">
        <ResultLink dataset={dataset} renderTitle={true}></ResultLink>
        <a href={`?detail=${detail}`} className="content" onClick={onClick}>
          <div className="description">
            {dataset.description && <p>{dataset.description}</p>}
            <div className="link">
              <FiCornerDownRight />
              <span>Detailansicht</span>
            </div>
          </div>

          <div className="stats">
            {dataset.types && <p>Typ: {types}</p>}
            {dataset.license && <p>Lizenz: {dataset.license.label}</p>}
            {dataset.origins && <p>Herkunft: {origins}</p>}
            {tags && <p>Schlagwörter: {tags}</p>}
          </div>
        </a>
      </div>

      <div className="score-container">
        <Score $tooltip="auffindbar">
          <span>F</span>
          <ScoreValue $score={dataset.quality.findability.score} />
        </Score>
        <Score $tooltip="zugänglich">
          <span>A</span>
          <ScoreValue $score={dataset.quality.accessibility.score} />
        </Score>
        <Score $tooltip="interoperabel">
          <span>I</span>
          <ScoreValue $score={dataset.quality.interoperability.score} />
        </Score>
        <Score $tooltip="wiederverwendbar">
          <span>R</span>
          <ScoreValue $score={dataset.quality.reusability.score} />
        </Score>
      </div>
    </ResultWrapper>
  )
}

export function ResultLink({
  dataset,
  renderTitle = true,
}: {
  dataset: Dataset
  renderTitle?: boolean
}) {
  const handleClick = useCallback(() => {
    if (
      dataset.source_url_explainer &&
      ('CopyTitle' in dataset.source_url_explainer ||
        'CopyStationId' in dataset.source_url_explainer)
    ) {
      let textToCopy: string | null = null

      if ('CopyTitle' in dataset.source_url_explainer) {
        textToCopy = dataset.title
      } else if (
        'CopyStationId' in dataset.source_url_explainer &&
        dataset.types
      ) {
        const nonStringTypes = dataset.types.filter(
          (t) => typeof t !== 'string',
        ) as Exclude<Type, string>[]

        for (const t of nonStringTypes) {
          if ('Measurements' in t && t.Measurements?.station?.id) {
            textToCopy = t.Measurements.station.id
            break
          }
        }
      }

      if (textToCopy !== null) {
        navigator.clipboard
          .writeText(textToCopy)
          .then(() => console.log('Copied to clipboard:', textToCopy))
          .catch((err) => console.error('Clipboard write failed:', err))
      }
    }
  }, [dataset])

  const sourceURLText = dataset.source_url_explainer
    ? Object.values(dataset.source_url_explainer)?.[0]
    : dataset.quality?.accessibility?.landing_page === 'Generic'
      ? 'Übersichtsseite (kein Direktlink zum Ergebnis vorhanden)'
      : 'Direktlink'

  if (renderTitle) {
    return (
      <a
        href={dataset.source_url}
        className="title"
        onClick={handleClick}
        onAuxClick={handleClick}
      >
        <h2>{dataset.title}</h2>
        <div className="link">
          <FiExternalLink />
          <span>{sourceURLText}</span>
        </div>
      </a>
    )
  } else {
    return (
      <>
        <div className="source_url">
          {dataset.source_url_explainer && (
            <>
              <FiAlertTriangle></FiAlertTriangle>
              <span>Für dieses Ergebnis ist kein direkter Link vorhanden.</span>
            </>
          )}
          <p>
            <span>{sourceURLText}</span>
            <span>: </span>
            <a
              href={dataset.source_url}
              onClick={handleClick}
              onAuxClick={handleClick}
            >
              {dataset.source_url}
            </a>
          </p>
        </div>
      </>
    )
  }
}

function ShareButton() {
  const { share, copied, canShare } = useShare()

  // on mobile we show the share icon
  // on desktop we show the copy icon
  // when the url was copied (on desktop) we show the copied icon
  const showShareIcon = canShare
  const showCopyIcon = !canShare && !copied
  const showCopiedIcon = !canShare && copied

  return (
    <ShareButtonWrapper
      className="ShareButton"
      onClick={share}
      $copied={copied}
      title={
        canShare
          ? 'Link zur Suchanfrage teilen'
          : 'Link zur Suchanfrage in die Zwischenablage kopieren'
      }
    >
      {showShareIcon && <FiShare2 size={18} />}
      {showCopyIcon && <FiCopy size={18} />}
      {showCopiedIcon && <FiCheck size={18} />}
    </ShareButtonWrapper>
  )
}

function useShare() {
  const [copied, setCopied] = useState(false)
  const canShare = !!navigator.share

  /**
   * share on mobile and copy url on desktop
   */
  const share = () => {
    if (navigator.share) {
      navigator.share({
        url: window.location.href,
      })
    } else {
      navigator.clipboard.writeText(window.location.href)
      setCopied(true)
      setTimeout(() => setCopied(false), 1000)
    }
  }

  return { share, copied, canShare }
}

const ResultContainer = styled.div`
  grid-area: results;
  min-width: 0;
  display: flex;
  flex-direction: column;
  gap: ${theme.sizing[`7`]};
`

const ResultContext = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: ${theme.sizing[`5`]};
  #result-count {
    ${theme.ty('base/sm bold', 'base/md bold')};
  }
  .external-link {
    @media (min-width: ${theme.ms.LG}px) {
      display: flex;
    }
    display: none;
  }
  .show-filter {
    @media (min-width: ${theme.ms.LG}px) {
      display: none;
    }
    display: flex;
  }
  a.external-link,
  button {
    color: ${theme.colors.black};
    background: ${theme.colors['neutral-200']};
    width: 45px;
    height: 38px;
    border: 1px solid ${theme.colors['neutral-300']};
    display: flex;
    position: relative;
    align-items: center;
    justify-content: center;
  }
  .spacer {
    flex: 1 1 0%;
  }
`

const ResultsList = styled.div`
  max-width: 1650px;
  display: flex;
  flex-direction: column;
  gap: ${theme.sizing[`7`]};
  padding: 0;
  > .external-link {
    display: none;

    @media (min-width: ${theme.ms.LG}px) {
      color: ${theme.colors.black};
      background: ${theme.colors.white};
      width: 45px;
      height: 38px;
      border: 1px solid ${theme.colors['neutral-300']};
      display: flex;
      position: relative;
      align-items: center;
      justify-content: center;
    }
  }
  .explainer {
    ${theme.ty('base/sm', 'base/md')};

    a {
      color: ${theme.colors['primary-900']};
    }

    p {
      margin: revert;
    }
  }
`

const ResultWrapper = styled.article`
  padding: ${theme.sizing['7']};
  border: 1px solid ${theme.colors['neutral-600']};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: ${theme.sizing[`10`]};

  @media (min-width: ${theme.ms.SM}px) {
    flex-direction: row;
  }
  .score-container {
    width: 130px;
  }
  .container {
    display: flex;
    flex-direction: column;
    ${theme.ty('base/base')}
    gap: ${theme.sizing[`5`]};
    min-width: 0;
    > a {
      padding: ${theme.sizing[`2.5`]};
      margin: -${theme.sizing[`2.5`]};
    }
  }
  a.title {
    color: ${theme.colors['primary-900']};
    text-decoration-color: ${theme.colors['primary-900']};

    > svg {
      margin-left: ${theme.sizing[`1.5`]};
      vertical-align: middle;
    }

    &:hover {
      text-decoration: none;
      > .link {
        font-weight: 900;
        text-decoration: none;

        > svg {
          scale: 1.1;
        }
      }
    }
  }
  a.content {
    &:hover {
      .description > .link {
        font-weight: 900;
        text-decoration: none;

        > svg {
          scale: 1.1;
        }
      }
      cursor: pointer;
    }
  }
  .description > p {
    ${theme.ty('base/sm', 'base/base')};
    display: inline;
    color: black;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    overflow: hidden;
    margin: 0;
    box-sizing: border-box;
  }
  .content {
    text-decoration: none;
    display: flex;
    flex-direction: column;
    gap: ${theme.sizing[`5`]};
  }
  .link {
    ${theme.ty('base/sm', 'base/base')};
    color: ${theme.colors['primary-900']};
    text-decoration: underline;
    text-underline-position: under;
    margin: 0;
    display: inline-block;

    > svg,
    span {
      margin: 0 ${theme.sizing[`1`]};
      vertical-align: middle;
    }

    > svg {
      min-width: 11px;
      stroke: ${theme.colors['primary-900']};
    }
  }
  h2 {
    ${theme.ty('display/base', 'display/md bold')};
    margin: 0;
  }
  .stats {
    display: flex;
    flex-direction: column;
    gap: ${theme.sizing['1.5']};
    color: ${theme.colors['neutral-700']};
    ${theme.ty('display/xs regular', 'display/sm regular')}
    > p {
      margin: 0;
    }
  }
`

const Score = styled.div<{ $tooltip: string }>`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: ${theme.sizing['1.5']};
  border-bottom: 1px solid ${theme.colors['neutral-300']};
  margin-bottom: ${theme.sizing['1.5']};
  ${theme.ty('display/sm bold')}
  color: ${theme.colors['primary-900']};
  gap: ${theme.sizing['2.5']};

  &:hover {
    &::after {
      content: '${(props) => props.$tooltip}';
      position: absolute;
      top: -${theme.sizing['2.5']};
      right: calc(100% + ${theme.sizing['2.5']});
      z-index: 1;
      background: ${theme.colors['neutral-800']};
      padding: ${theme.sizing['2.5']};
      ${theme.ty('base/base')};
      color: ${theme.colors.white};
    }
  }
`

const ScoreValue = styled.div<{ $score: number }>`
  width: 110px;
  height: 12px;
  background: ${theme.colors['neutral-200']};
  position: relative;

  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: ${(props) => props.$score * 100}%;
    background: ${theme.colors['primary-500']};
  }
`

const ShareButtonWrapper = styled.button<{ $copied: boolean }>`
  ${(p) =>
    p.$copied &&
    css`
      && {
        background: ${theme.colors['secondary-800']};
        color: ${theme.colors.white};
      }
    `}
`
