import { triggerSearch } from '../store'

import { ResultsForm } from './App'
import Query from './Query'

export default function HomepageApp() {
  return (
    <ResultsForm onSubmit={onSubmit}>
      <Query homepage={true} />
    </ResultsForm>
  )
}

function onSubmit(event: React.FormEvent<HTMLFormElement>) {
  event.preventDefault()

  triggerSearch(true)
}
