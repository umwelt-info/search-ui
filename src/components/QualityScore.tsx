import styled from 'styled-components'
import { FiHelpCircle } from 'react-icons/fi'

import theme from '../theme'
import { Quality } from '../api'

export default function QualityScore(quality: Quality) {
  const boolToPercentScore = (bool: boolean) => (bool ? 100 : 0)
  const numToPercentScore = (num: number) => num * 100

  const { findability, accessibility, interoperability, reusability } = quality

  return (
    <QualityWrapper className="QualityScore" key="quality_score">
      <h2>
        Metadatenqualität
        <a
          className="internal-link"
          href="/artikel/ueber-die-metadatenqualitaet"
          title="Mehr zur Metadatenqualität"
          key="more_metadata_quality"
        >
          <FiHelpCircle size={18} />
        </a>
      </h2>
      <div className="scores">
        <ScoreDetail
          title="auffindbar"
          percentScore={numToPercentScore(findability.score)}
          subScores={[
            {
              label: 'Kennung',
              percentScore: boolToPercentScore(findability.identifier),
              tooltip: 'Hat der Inhalt eine eindeutige Kennung?',
            },
            {
              label: 'Titel',
              percentScore: numToPercentScore(findability.title),
              tooltip: 'Ist der Titel gut lesbar?',
            },
            {
              label: 'Beschreibung',
              percentScore: numToPercentScore(findability.description),
              tooltip: 'Ist die Beschreibung gut lesbar?',
            },
            {
              label: 'Schlagwörter',
              percentScore: numToPercentScore(findability.keywords),
              tooltip:
                'Wie hoch ist der Anteil an Schlagwörtern, die im Umweltthesaurus vorhanden sind?',
            },
            {
              label: 'Raumbezug',
              percentScore: numToPercentScore(findability.spatial_score),
              tooltip: 'Wie präzise ist der Ortsbezug?',
            },
            {
              label: 'Zeitbezug',
              percentScore: numToPercentScore(findability.temporal),
              tooltip: 'Ist ein Zeitbezug vorhanden?',
            },
          ]}
        />

        <ScoreDetail
          title="zugänglich"
          percentScore={numToPercentScore(accessibility.score)}
          subScores={[
            {
              label: 'Direktlink',
              percentScore: numToPercentScore(accessibility.landing_page_score),
              tooltip: 'Führt der Link direkt zum Angebot?',
            },
            {
              label: 'Direktzugriff',
              percentScore: boolToPercentScore(accessibility.direct_access),
              tooltip: 'Ist es möglich den Inhalt direkt herunterzuladen?',
            },
            {
              label: 'öffentlich',
              percentScore: boolToPercentScore(
                accessibility.publicly_accessible,
              ),
              tooltip: 'Ist der Link ohne Anmeldung nutzbar?',
            },
          ]}
        />

        <ScoreDetail
          title="interoperabel"
          percentScore={numToPercentScore(interoperability.score)}
          subScores={[
            {
              label: 'Maschinenlesbare Daten',
              percentScore: boolToPercentScore(
                interoperability.machine_readable_data,
              ),
              tooltip: 'Ist der Inhalt automatisiert lesbar?',
            },
            {
              label: 'Maschinenlesbare Metadaten',
              percentScore: boolToPercentScore(
                interoperability.machine_readable_metadata,
              ),
              tooltip: 'Sind die Metadaten automatisiert lesbar?',
            },
            {
              label: 'Medientyp',
              percentScore: boolToPercentScore(interoperability.media_type),
              tooltip: 'Ist das Dateiformat des Inhaltes bekannt?',
            },
            {
              label: 'Offenes Dateiformat',
              percentScore: boolToPercentScore(
                interoperability.open_file_format,
              ),
              tooltip: 'Ist das Dateiformat des Inhaltes offen (z.B. .CSV)?',
            },
          ]}
        />

        <ScoreDetail
          title="wiederverwendbar"
          percentScore={numToPercentScore(reusability.score)}
          subScores={[
            {
              label: 'Lizenz',
              percentScore: numToPercentScore(reusability.license_score),
              tooltip: 'Ist die Lizenz bekannt und frei?',
            },
            {
              label: 'Kontakt',
              percentScore: boolToPercentScore(reusability.contact_info),
              tooltip: 'Sind Kontaktinformationen vorhanden?',
            },
            {
              label: 'Veröffentlicher',
              percentScore: boolToPercentScore(reusability.publisher_info),
              tooltip: 'Ist der*die Herausgeber*in bekannt?',
            },
          ]}
        />
      </div>
    </QualityWrapper>
  )
}

type ScoreDetailProps = {
  title: string
  percentScore: number
  subScores: {
    label: string
    percentScore: number | null
    tooltip: string
  }[]
}

function ScoreDetail(scoreDetailProps: ScoreDetailProps) {
  return (
    <QualityDetailWrapper className="ScoreDetail" key={scoreDetailProps.title}>
      <summary>
        <h3>{scoreDetailProps.title}</h3>
        <div className="score" key="score_summary">
          {scoreDetailProps.percentScore.toFixed(0)}
        </div>
        <div className="score-label" key="score_summary_label">
          Punkte
        </div>
      </summary>

      <div className="sub-scores">
        {scoreDetailProps.subScores.map((subScore) => {
          let classes
          let width
          let label

          if (subScore.percentScore !== null) {
            classes = 'label'
            width = subScore.percentScore
            label = subScore.percentScore.toFixed(0)
          } else {
            classes = 'label missing'
            width = 0
            label = 'N/A'
          }

          return (
            <SubScore key={subScore.label} $tooltip={subScore.tooltip}>
              <div className={classes}>{subScore.label}</div>
              <div className="score" key={subScore.label + 'score'}>
                <ProgressBar $percentScore={width} />
                <div className={classes}>{label}</div>
              </div>
            </SubScore>
          )
        })}
      </div>
    </QualityDetailWrapper>
  )
}

const QualityWrapper = styled.div`
  > h2 {
    ${theme.ty('display/base bold')}
    color: ${theme.colors['neutral-800']};
    margin: 0;
    margin-bottom: ${theme.sizing['2.5']};
    display: flex;
    > .internal-link {
      color: ${theme.colors['primary-900']};
      margin-left: ${theme.sizing['1']};
    }
  }

  > .scores {
    display: grid;
    gap: ${theme.sizing['2.5']};
    grid-template-columns: minmax(0, 1fr);

    @media (min-width: ${theme.ms.SM}px) {
      grid-template-columns: repeat(2, minmax(0, 1fr));
    }

    @media (min-width: ${theme.ms.XL}px) {
      grid-template-columns: repeat(4, minmax(0, 1fr));
    }
  }
`

const QualityDetailWrapper = styled.details`
  background: ${theme.colors.white};
  padding: ${theme.sizing['3']};
  display: flex;
  flex-direction: column;
  > summary {
    cursor: pointer;
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;

    > h3 {
      ${theme.ty('display/base bold')}
      color: ${theme.colors['primary-900']};
      margin: 0;
      margin-bottom: ${theme.sizing['2.5']};
      text-align: center;
      hyphens: none;
    }

    > .score {
      width: 50px;
      height: 50px;
      border-radius: 50%;
      background: ${theme.colors['primary-300']};
      display: grid;
      place-items: center;
      ${theme.ty('display/md bold')}
      color: ${theme.colors['primary-900']};
      margin-bottom: ${theme.sizing['1']};
    }

    > .score-label {
      ${theme.ty('display/xs bold')}
      color: ${theme.colors['neutral-700']};
      margin: 0;
      margin-bottom: ${theme.sizing['5']};
      color: ${theme.colors['primary-900']};
    }
  }

  > .sub-scores {
    width: 100%;
    display: flex;
    flex-direction: column;
    gap: ${theme.sizing['2.5']};
  }
`

const SubScore = styled.div<{ $tooltip: string }>`
  position: relative;
  border-bottom: 1px solid ${theme.colors['neutral-300']};

  &:last-child {
    border-bottom: none;
  }

  &:hover {
    &::after {
      content: '${(props) => props.$tooltip}';
      position: absolute;
      top: -${theme.sizing['2.5']};
      right: calc(100% + ${theme.sizing['2.5']});
      z-index: 1;
      background: ${theme.colors['neutral-800']};
      padding: ${theme.sizing['2.5']};
      ${theme.ty('base/base')};
      color: ${theme.colors.white};
    }
  }

  > .label {
    ${theme.ty('display/xs regular')}
    color: ${theme.colors['primary-900']};
  }

  > .missing {
    color: ${theme.colors['neutral-300']};
  }

  > .score {
    display: flex;
    align-items: center;
    margin-top: ${theme.sizing['1']};
    margin-bottom: ${theme.sizing['2.5']};

    > .label {
      ${theme.ty('display/xs regular')}
      color: ${theme.colors['primary-900']};
      width: 35px;
      text-align: right;
    }

    > .missing {
      color: ${theme.colors['neutral-300']};
    }
  }
`

const ProgressBar = styled.div<{ $percentScore: number }>`
  flex: 1;
  background: ${theme.colors['neutral-200']};
  height: 8px;
  position: relative;

  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: ${(props) => props.$percentScore}%;
    height: 100%;
    background: ${theme.colors['primary-500']};
  }
`
