import styled, { css } from 'styled-components'
import { useMemo } from 'react'
import { FiArrowRight, FiArrowLeft } from 'react-icons/fi'

import theme from '../theme'
import { useStore, setPage, triggerSearch } from '../store'
import { RESULTS_PER_PAGE } from '../config'
import { computePaginationOptions } from '../utils'

export default function Pagination() {
  const page = useStore((store) => store.page)
  const pages = useStore((store) => store.results.pages)
  const count = useStore((store) => store.results.count)

  const from = (page - 1) * RESULTS_PER_PAGE + 1
  const to = Math.min(page * RESULTS_PER_PAGE, pages)

  const options = useMemo(
    () => computePaginationOptions(page, pages),
    [page, pages],
  )

  return (
    <PaginationWrapper id="pagination">
      <div className="value">
        {from.toLocaleString()} bis {to.toLocaleString()} von{' '}
        {count !== undefined && count.toLocaleString()}
      </div>

      <div className="options">
        {page !== 1 && (
          <PageButton type="button" onClick={() => onClick(page - 1)}>
            <FiArrowLeft />
          </PageButton>
        )}

        {options.map((option) => (
          <PageButton
            key={`page-${option}`}
            type="button"
            onClick={() => onClick(option)}
            $selected={option === page}
          >
            {option.toLocaleString()}
          </PageButton>
        ))}

        {page !== pages && (
          <PageButton type="button" onClick={() => onClick(page + 1)}>
            <FiArrowRight />
          </PageButton>
        )}
      </div>
    </PaginationWrapper>
  )
}

function onClick(page: number) {
  setPage(page)

  triggerSearch()

  // Scroll to a bit above the results
  const results = document.getElementById('results')
  window.scrollTo({ top: results!.offsetTop - 100, behavior: 'smooth' })
}

const PaginationWrapper = styled.nav`
  grid-area: pagination;
  min-width: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  gap: ${theme.sizing['2.5']};

  @media (min-width: ${theme.ms.SM}px) {
    flex-direction: row;
  }

  > .value {
    ${theme.ty('base/md bold')}
    color: ${theme.colors['neutral-800']};
  }

  > .options {
    display: flex;
  }
`

const PageButton = styled.button<{ $selected?: boolean; $small?: boolean }>`
  ${theme.ty('display/sm bold')}
  min-width: ${({ $small }) => ($small ? '30px' : '40px')};
  color: ${theme.colors['black']};
  background: none;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${theme.colors['neutral-700']};
  border-left: none;
  cursor: pointer;
  &:first-of-type {
    border-left: 1px solid ${theme.colors['neutral-700']};
  }
  > svg {
    transform: scale(1.8);
  }

  ${({ $selected }) =>
    $selected &&
    css`
      color: ${theme.colors['white']};
      background: ${theme.colors['secondary-800']};
    `}
`
