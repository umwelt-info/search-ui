import styled, { css } from 'styled-components'
import { useCallback, Fragment } from 'react'
import { FiCornerDownRight, FiX } from 'react-icons/fi'

import theme from '../theme'
import {
  Store,
  useStore,
  setPage,
  AnyRoot,
  setAnyRoot,
  triggerSearch,
} from '../store'
import { ROOT_LABELS } from '../labels'

export default function RootFilter({
  root,
  options,
}: {
  root: AnyRoot
  options: [string, number][]
}) {
  const value = useStore((store) => store[root])
  const searchedValue = useStore(
    (store) =>
      store[
        ('searched' +
          root.slice(0, 1).toUpperCase() +
          root.slice(1)) as keyof Store
      ] as string,
  )

  const valuePath = value.slice(1).split('/').filter(Boolean)
  const searchedValuePath = searchedValue.slice(1).split('/').filter(Boolean)

  const onClickValue = useCallback(
    (index: number) => {
      setAnyRoot(root, '/' + searchedValuePath.slice(0, index).join('/'))
      setPage(1)

      triggerSearch()
    },
    [root, searchedValuePath],
  )

  const onClickOption = useCallback(
    (option: string) => {
      setAnyRoot(root, option)
      setPage(1)

      triggerSearch()
    },
    [root],
  )

  return (
    <RootFilterWrapper>
      <h2>{ROOT_LABELS[root]}</h2>

      {valuePath.length !== 0 && (
        <div className="value">
          {valuePath.map((value, index) => (
            <Fragment key={value}>
              {index !== 0 && <span className="divider">\</span>}

              <ValueButton
                key={`value-${index}`}
                type="button"
                onClick={() => onClickValue(index)}
                $removed={index >= searchedValuePath.length}
                $path={index !== valuePath.length - 1}
              >
                {value} <FiX size={20} />
              </ValueButton>
            </Fragment>
          ))}
        </div>
      )}

      {options.length !== 0 && (
        <div className="options-wrapper">
          {valuePath.length !== 0 && (
            <div className="sub-icon">
              <FiCornerDownRight size={24} />
            </div>
          )}
          <div className="options">
            {options.map(([option, count], index) => (
              <OptionButton
                key={`option-${index}`}
                type="button"
                onClick={() => onClickOption(option)}
              >
                {option.slice(1).split('/').pop()} ({count.toLocaleString()})
              </OptionButton>
            ))}
          </div>
        </div>
      )}
    </RootFilterWrapper>
  )
}

const RootFilterWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${theme.sizing['8']};
  padding-bottom: 20px;
  border-bottom: 1px solid ${theme.colors['neutral-600']};
  .options-wrapper {
    display: flex;

    > .sub-icon > svg {
      color: ${theme.colors['secondary-800']};
      margin-right: ${theme.sizing['2.5']};
    }
  }
  .value,
  .options {
    display: flex;
    flex-wrap: wrap;
    gap: ${theme.sizing['2.5']};
    align-items: center;
  }
  .value {
    align-items: center;
    gap: ${theme.sizing['2.5']} ${theme.sizing['1.5']};
  }
  .divider {
    ${theme.ty('display/sm regular')}
    margin: 0;
    color: ${theme.colors['black']};
  }
`

const OptionButton = styled.button<{
  $selected?: boolean
  $removed?: boolean
  $path?: boolean
}>`
  ${theme.ty('display/sm regular')}
  cursor: pointer;
  padding: ${theme.sizing['1.5']} ${theme.sizing['3']};
  background: ${theme.colors['neutral-200']};
  border: 1px solid ${theme.colors['neutral-300']};
  color: ${theme.colors['neutral-700']};
  display: flex;
  align-items: center;
  gap: ${theme.sizing['1.5']};

  // selected style
  ${({ $selected }) =>
    $selected &&
    css`
      ${theme.ty('display/sm bold')}
      background: ${theme.colors['secondary-800']};
      border: none;
      color: ${theme.colors['white']};
    `}
`

const ValueButton = styled(OptionButton)<{ $removed: boolean; $path: boolean }>`
  ${theme.ty('display/sm bold')}
  background: ${theme.colors['secondary-800']};
  border: none;
  color: ${theme.colors['white']};

  ${({ $path }) =>
    $path === true &&
    css`
      background: ${theme.colors['secondary-300']};
      color: ${theme.colors['black']};
      ${theme.ty('display/sm regular')}
    `}
`
