import styled from 'styled-components'
import { useState, Fragment, useRef, useEffect } from 'react'
import { FiAlertTriangle, FiArrowLeft, FiMessageCircle } from 'react-icons/fi'

import { Dataset, TimeRange } from '../api'
import theme from '../theme'
import { RegionDetailsMap } from '../leaflet'
import { STATUS_LABELS } from '../labels'
import { useStore, setDetail, triggerDetail } from '../store'

import DetailAccordion from './DetailAccordion'
import QualityScore from './QualityScore'
import { ResultLink } from './Results'

export default function Details({ dataset }: { dataset: Dataset }) {
  const detail = useStore((store) => store.detail)

  const leafletId = dataset.bounding_boxes
    ? RegionDetailsMap(dataset.bounding_boxes)
    : undefined

  const feedbackMessage = `Hinweis zum Suchergebnis ${detail}:`
  const feedbackLink = `/feedback-geben?subject=dataset&message=${feedbackMessage}`

  return (
    <>
      <DetailsWrapper id="detail" key="detail">
        <DetailContent>
          <h1>{dataset.title}</h1>

          {dataset.status === 'Obsolete' && (
            <div className="status-warning" key="status_warning">
              <FiAlertTriangle size={20} /> &nbsp;{' '}
              <span className="title">Veraltet</span> &nbsp;-&nbsp;Diese
              Information wurde von der Quelle als veraltet markiert.
            </div>
          )}

          {Description(dataset.description)}

          <ResultLink dataset={dataset} renderTitle={false} />

          <p>
            <a className="feedback" href={feedbackLink}>
              <FiMessageCircle size={20} />
              Feedback zum Suchergebnis geben
            </a>
          </p>

          <DetailAccordion dataset={dataset} />

          <div key="quality_main">{QualityScore(dataset.quality)}</div>
        </DetailContent>
      </DetailsWrapper>
      <DetailBackLink />
      <Meta>
        {dataset.bounding_boxes && (
          <div className="meta-item" key="bounding_boxes">
            <h2>Region</h2>
            <Map id={leafletId} />
          </div>
        )}
        {(dataset.time_ranges || dataset.issued || dataset.modified) && (
          <div className="meta-item" key="time_ranges">
            <div>
              <h2 key="tr_title">Zeitraum</h2>
              {dataset.time_ranges?.map((time_range) =>
                formatTimeRange(time_range, dataset.issued, dataset.modified),
              )}
              {dataset.issued && (
                <p>erstellt: {new Date(dataset.issued).toLocaleDateString()}</p>
              )}
              {dataset.modified && (
                <p>
                  aktualisiert:{' '}
                  {new Date(dataset.modified).toLocaleDateString()}
                </p>
              )}
            </div>
          </div>
        )}
        <div className="meta-item" key="license">
          <h2>Lizenz</h2>
          {dataset.license.url ? (
            <a href={dataset.license.url}>{dataset.license.label}</a>
          ) : (
            dataset.license.label
          )}
          {dataset.mandatory_registration && (
            <>
              <br />
              (Registrierung erforderlich)
            </>
          )}
        </div>
        <div className="meta-item" key="status">
          <h2>Sprache</h2>
          {dataset.language.label}
        </div>
        {!dataset.status.includes('Active') && (
          <div className="meta-item">
            <h2>Status</h2>
            {STATUS_LABELS[dataset.status]}
          </div>
        )}
      </Meta>
    </>
  )
}

function Description(description: string | undefined) {
  const [isOpen, setIsOpen] = useState(false)
  const [isOverflowing, setIsOverflowing] = useState(false)
  const descriptionRef = useRef<HTMLParagraphElement | null>(null)

  useEffect(() => {
    const checkOverflow = () => {
      if (descriptionRef.current) {
        const { scrollHeight, offsetHeight } = descriptionRef.current
        setIsOverflowing(scrollHeight > offsetHeight)
      }
    }

    // Run the check on mount and when the window resizes
    checkOverflow()
    window.addEventListener('resize', checkOverflow)
    return () => window.removeEventListener('resize', checkOverflow)
  }, [description])

  return (
    <div>
      <DescriptionContainer
        lines={16}
        className={`description-container ${isOpen ? 'open' : 'closed'}`}
      >
        <p className="description" ref={descriptionRef}>
          {description}
        </p>
      </DescriptionContainer>
      {isOverflowing && (
        <ReadMoreButton type="button" onClick={() => setIsOpen(!isOpen)}>
          {isOpen ? 'Weniger anzeigen' : 'Mehr anzeigen'}
        </ReadMoreButton>
      )}
    </div>
  )
}

function DetailBackLink() {
  return (
    <DetailBackLinkWrapper
      type="button"
      className="DetailBackLink"
      onClick={onClickBackLink}
      title="detailBackLink"
    >
      <FiArrowLeft size={20} /> Suchergebnisse
    </DetailBackLinkWrapper>
  )
}

function onClickBackLink() {
  setDetail(null)

  triggerDetail()
}

function formatTimeRange(
  time_range: TimeRange,
  issued?: string,
  modified?: string,
) {
  const from = new Date(time_range.from)
  const until = new Date(time_range.until)

  const issuedTime = issued ? new Date(issued).getTime() : null
  const modifiedTime = modified ? new Date(modified).getTime() : null

  if (
    from.getTime() === until.getTime() &&
    (from.getTime() === issuedTime || from.getTime() === modifiedTime)
  ) {
    return null
  }

  const fromLabel = from.toLocaleDateString()
  const untilLabel = until.toLocaleDateString()

  const label =
    from.getTime() !== until.getTime()
      ? `${fromLabel} - ${untilLabel}`
      : fromLabel

  return (
    <Fragment key={from.getTime()}>
      <p>{label}</p>
    </Fragment>
  )
}

const DescriptionContainer = styled.div<{ lines: number }>`
  display: -webkit-box;
  -webkit-box-orient: vertical;
  overflow: hidden;
  &.open {
    height: auto;
  }
  &.closed {
    -webkit-line-clamp: ${(props) => props.lines};
    text-overflow: ellipsis;
  }
`
const ReadMoreButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  ${theme.ty('base/base bold')}
  color: ${theme.colors['primary-900']};
  display: flex;
  align-items: center;
  text-decoration: underline;
`

const DetailBackLinkWrapper = styled.button`
  background: none;
  grid-area: detail-backlink;
  border: none;
  cursor: pointer;
  ${theme.ty('display/sm bold')}
  color: ${theme.colors['secondary-800']};
  display: flex;
  align-items: center;

  > svg {
    margin-right: ${theme.sizing['1.5']};
  }
`

const DetailsWrapper = styled.div`
  grid-area: detail;
  min-width: 0;
`

const DetailContent = styled.div`
  min-width: 0;
  display: flex;
  flex-direction: column;
  gap: ${theme.sizing['5']};
  h1 {
    ${theme.ty('display/md bold', 'display/xl bold')}
    color: ${theme.colors.black};
    margin: 0;
  }
  p,
  li,
  span {
    ${theme.ty('base/base', 'base/md')}
    color: ${theme.colors.black};
    margin: 0;
  }

  a {
    ${theme.ty('base/base', 'base/md')}
    color: ${theme.colors['primary-900']};
  }
  p:has(.feedback) {
    margin: ${theme.sizing['5']} 0;
  }
  .feedback {
    ${theme.ty('display/base bold')}
    cursor: pointer;
    padding: ${theme.sizing['2.5']} ${theme.sizing['5']};
    background: ${theme.colors['secondary-800']};
    border: none;
    color: white;
    width: fit-content;
    text-decoration: none;
    box-sizing: border-box;
    display: flex;
    gap: ${theme.sizing['2.5']};
    align-items: center;
  }
`

const Meta = styled.div`
  grid-area: meta;
  min-width: 0;
  display: flex;
  flex-direction: column;
  gap: ${theme.sizing['2.5']};
  ${theme.ty('display/base')}
  color: ${theme.colors['neutral-800']};

  .meta-item {
    display: flex;
    flex-direction: column;
    gap: ${theme.sizing['5']};
    padding-bottom: ${theme.sizing['8']};
    border-bottom: 1px solid ${theme.colors['neutral-600']};

    > h2 {
      ${theme.ty('display/base bold')}
      color: ${theme.colors['neutral-800']};
      margin: 0;
    }
  }
`
const Map = styled.div`
  height: 350px;

  > #leaflet-detail-map {
    height: 100%;
    width: 100%;
  }

  .leaflet-div-icon {
    top: -18px;
    left: -18px;
    color: ${theme.colors['primary-900']};

    background: none;
    border: none;

    > svg {
      height: 36px;
      width: 36px;

      stroke-width: 2;

      opacity: 0.7;
      fill-opacity: 0.3;
    }
  }
`
