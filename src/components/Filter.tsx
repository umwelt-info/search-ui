import styled from 'styled-components'
import { FiCheck } from 'react-icons/fi'

import {
  triggerSearch,
  setPage,
  useStore,
  setAnyRoot,
  toggleMobileFilterDrawer,
} from '../store'
import theme from '../theme'

import RootFilter from './RootFilter'
import DateFilter from './DateFilter'
import RegionFilter from './RegionFilter'

export default function Filter() {
  const licenses = useStore((store) => store.results.licenses)
  const origins = useStore((store) => store.results.origins)
  const types = useStore((store) => store.results.types)
  const resourceTypes = useStore((store) => store.results.resource_types)
  const languages = useStore((store) => store.results.languages)
  const topics = useStore((store) => store.results.topics)

  const mobileFilterDrawerOpen = useStore(
    (store) => store.mobileFilterDrawerOpen,
  )

  return (
    <FilterWrapper className="Filter" $open={mobileFilterDrawerOpen}>
      <div className="content">
        <MobileFilterHead />
        <div className="title">
          <h2>Filter</h2>
          <button onClick={() => onClickReset()}>zurücksetzen</button>
        </div>
        <div className="region-filter">
          <RegionFilter />
        </div>
        <DateFilter />
        <RootFilter root="licensesRoot" options={licenses} />
        <RootFilter root="originsRoot" options={origins} />
        <RootFilter root="typesRoot" options={types} />
        <RootFilter root="resourceTypesRoot" options={resourceTypes} />
        <RootFilter root="languagesRoot" options={languages} />
        <RootFilter root="topicsRoot" options={topics} />
        <MobileFilterHead />
      </div>
    </FilterWrapper>
  )
}

function onClickReset() {
  setAnyRoot('licensesRoot', '/')
  setAnyRoot('originsRoot', '/')
  setAnyRoot('typesRoot', '/')
  setAnyRoot('resourceTypesRoot', '/')
  setAnyRoot('languagesRoot', '/')
  setAnyRoot('topicsRoot', '/')

  useStore.setState({
    boundingBox: null,
    timeRangeFrom: null,
    timeRangeUntil: null,
  })

  setPage(1)

  triggerSearch()
}

function MobileFilterHead() {
  return (
    <MobileFilterHeadWrapper className="MobileFilterHead">
      <div className="row">
        <button
          className="close"
          onClick={toggleMobileFilterDrawer}
          title="Filter übernehmen und schließen"
        >
          <FiCheck size={30} />
        </button>
      </div>
    </MobileFilterHeadWrapper>
  )
}

const FilterWrapper = styled.nav<{ $open: boolean }>`
  position: fixed;
  background: white;
  z-index: 5; // sticky-header has z-index 4
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  overflow-x: auto;
  transform: ${({ $open }) => ($open ? 'translateX(0)' : 'translateX(-100%)')};
  transition: transform 0.3s ease-in-out;

  @media (min-width: ${theme.ms.LG}px) {
    position: initial;
    grid-area: filter;
    min-width: 0;
    transform: none;
    background: none;
  }

  .title {
    display: flex;
    gap: ${theme.sizing['5']};
    flex-direction: column;
  }
  h2 {
    ${theme.ty('display/base', 'display/md bold')};
    margin: 0;
  }
  .title > button {
    background: none;
    border: none;
    padding: 0;
    text-align: left;
    cursor: pointer;
    text-decoration: underline;
    ${theme.ty('base/base')};
    color: ${theme.colors['primary-900']};
  }
  .content {
    padding: 0 ${theme.sizing['5']};
    display: flex;
    flex-direction: column;
    gap: ${theme.sizing['5']};
  }

  .region-filter {
    padding-bottom: 20px;
    border-bottom: 1px solid ${theme.colors['neutral-600']};
  }
`

const MobileFilterHeadWrapper = styled.div`
  text-align: end;
  padding: ${theme.sizing['5']};
  display: flex;
  flex-direction: column;
  gap: ${theme.sizing['5']};

  @media (min-width: ${theme.ms.LG}px) {
    display: none;
  }

  > .row {
    > button {
      background: none;
      border: none;
      cursor: pointer;
      color: ${theme.colors['neutral-700']};
    }
  }
`
