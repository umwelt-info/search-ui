import styled from 'styled-components'

import {
  useStore,
  triggerSearch,
  triggerDetail,
  setDetail,
  setPage,
} from '../store'
import theme from '../theme'
import { clearFocus } from '../utils'

import Query from './Query'
import Results from './Results'
import Pagination from './Pagination'
import Filter from './Filter'
import Details from './Details'

export default function App() {
  const dataset = useStore((store) => store.dataset)
  const count = useStore((store) => store.results.count)

  if (!dataset) {
    return (
      <ResultsForm onSubmit={onSubmit}>
        <Query />
        <Results />
        {count !== undefined && count > 0 && <Pagination />}
        <Filter />
      </ResultsForm>
    )
  } else {
    return (
      <DetailsForm onSubmit={onSubmit}>
        <Query />
        <Details dataset={dataset} />
      </DetailsForm>
    )
  }
}

function onSubmit(event: React.FormEvent<HTMLFormElement>) {
  event.preventDefault()

  setPage(1)
  setDetail(null)

  triggerSearch()
  triggerDetail()

  clearFocus()
}

export const ResultsForm = styled.form`
  width: 100%;
  padding: 0;
  display: grid;
  grid-gap: ${theme.sizing['5']};
  grid:
    'query'
    'replacement-terms'
    'results'
    'pagination'
    'filter'
    / 1fr;

  @media (min-width: ${theme.ms.LG}px) {
    grid:
      'query query'
      'replacement-terms replacement-terms'
      'filter results'
      'filter pagination'
      'filter .' 1fr
      / 380px 1fr;
  }
`

const DetailsForm = styled.form`
  display: grid;
  grid-gap: ${theme.sizing['5']};

  grid:
    'query'
    'replacement-terms'
    'detail-backlink'
    'detail'
    'meta'
    / 1fr;

  @media (min-width: ${theme.ms.LG}px) {
    grid:
      'query query query'
      'replacement-terms replacement-terms replacement-terms'
      'detail-backlink . detail' ${theme.sizing['8']}
      'meta . detail'
      / 380px 36px 1fr;
  }
`
