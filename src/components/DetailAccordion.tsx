import styled from 'styled-components'
import { useCallback, ReactElement, Fragment } from 'react'
import { FiChevronDown, FiDownload, FiExternalLink } from 'react-icons/fi'
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion'

import { Dataset, Tag, Origin, Resource, Type, Alternative } from '../api'
import {
  setQuery,
  useStore,
  triggerSearch,
  setDetail,
  triggerDetail,
} from '../store'
import theme from '../theme'
import { buildTypeLabel, DOMAIN_LABELS, REPORTING_OBLIGATIONS } from '../labels'

export default function DetailAccordion({ dataset }: { dataset: Dataset }) {
  const origins = useStore((store) => store.origins)

  return (
    <Accordion allowMultipleExpanded allowZeroExpanded preExpanded={['types']}>
      {origins &&
        origins.map((origin, index, origins) => (
          <DetailAccordionItem
            title={buildOriginTitle(origin, index, origins)}
            body={buildOriginDetails(origin)}
            key={`origin-${index}`}
            class={'origins'}
          />
        ))}
      {dataset.types && (
        <DetailAccordionItem
          title="Typ"
          body={dataset.types.map((type) => buildTypeDetails(type))}
          key="types"
          class="types"
        />
      )}
      {dataset.resources && (
        <DetailAccordionItem
          title="Ressourcen"
          body={dataset.resources.map((resource) => buildResource(resource))}
          key="resources"
          class="resources"
        />
      )}
      {dataset.tags && (
        <DetailAccordionItem
          title={'Schlagwörter'}
          body={dataset.tags.map((tag) => TagButton(tag))}
          key="tags"
          class="tags"
        />
      )}
      {dataset.alternatives && (
        <DetailAccordionItem
          title="Alternativer Datensatz"
          body={dataset.alternatives.map((alternative) =>
            buildAlternative(alternative),
          )}
          key="alternatives"
          class="alternatives"
        />
      )}
    </Accordion>
  )
}

type AccordionProps = {
  title: string
  body: ReactElement | ReactElement[]
  multiple?: boolean
  primary?: boolean
  key: string
  class: string
}

const DetailAccordionItem = (props: AccordionProps) => {
  return (
    <StyledAccordionItem>
      <AccordionItemHeading>
        <AccordionItemButton className="accordion-button" title={props.title}>
          <h2>{props.title}</h2>
          <div className="chevron">
            <FiChevronDown size={20} />
          </div>
        </AccordionItemButton>
      </AccordionItemHeading>

      <AccordionItemPanel className={'accordion-panel ' + props.class}>
        <div className={props.class}>{props.body}</div>
      </AccordionItemPanel>
    </StyledAccordionItem>
  )
}

function buildResource(resource: Resource) {
  return (
    <a className="link" href={resource.url} key={resource.url}>
      <span className="label">
        {resource.type.label === 'Webseite' ? (
          <FiExternalLink />
        ) : (
          <FiDownload />
        )}
        {resource.type.label}
      </span>
      <span>{resource.description ? resource.description : resource.url}</span>
    </a>
  )
}

function TagButton(tag: Tag) {
  const query = useStore((store) => store.query)
  const searchedQuery = useStore((store) => store.searchedQuery)

  const onClick = useCallback(
    (label: string) => {
      const labelLowerCase = label.toLowerCase()

      if (
        !searchedQuery.find(
          (clause) =>
            clause.field_name === 'tags' && clause.phrase === labelLowerCase,
        )
      ) {
        setQuery(query + ` +schlagwort:"${label}"`)
      }

      setDetail(null)

      triggerSearch()
      triggerDetail()
    },
    [query, searchedQuery],
  )

  let label

  if ('Other' in tag) {
    label = tag['Other']
  } else {
    label = tag['Umthes'].label
  }

  return (
    <button
      key={label}
      onClick={() => onClick(label)}
      title="Suche auf Schlagwort einschränken"
    >
      {label}
    </button>
  )
}

function buildOriginTitle(origin: Origin, index: number, origins: Origin[]) {
  if (origins.length > 1) {
    if (index === 0) {
      return `Primäre Herkunft: ${origin.name}`
    } else {
      return `Sekundäre Herkunft: ${origin.name}`
    }
  } else {
    return `Herkunft: ${origin.name}`
  }
}

function buildOriginDetails(origin: Origin) {
  return (
    <>
      <p>
        {origin.name}: {origin.description}
      </p>
      {origin.email && (
        <p>
          E-Mail: <a href={`mailto:${origin.email}`}>{origin.email}</a>
        </p>
      )}

      <p>
        <a className="link" href={origin.contact_url}>
          <FiExternalLink /> Kontaktformular
        </a>
      </p>
      {origin.about_url && (
        <p>
          <a className="link" href={origin.about_url}>
            {' '}
            <FiExternalLink /> Webseite
          </a>
        </p>
      )}
    </>
  )
}

function buildTypeDetails(type: Type): React.ReactElement {
  const label = buildTypeLabel(type)

  if (typeof type === 'object' && 'Taxon' in type) {
    const taxon = type.Taxon

    return (
      <Fragment key={label}>
        <h3>{label}</h3>
        <p>Wissenschaftlicher Name: {taxon.scientific_name}</p>
        {taxon.common_names.length > 0 && (
          <p>
            Deutscher Name: {taxon.common_names.map((name) => name).join('; ')}
          </p>
        )}
      </Fragment>
    )
  } else if (typeof type === 'object' && 'Measurements' in type) {
    const measurements = type.Measurements
    const station = measurements.station
    const measured_variables = measurements.measured_variables
    const methods = measurements.methods

    const domain = DOMAIN_LABELS[type['Measurements'].domain]

    return (
      <Fragment key={label}>
        <h3>
          {label} {domain && <>({domain})</>}
        </h3>
        {station && (
          <div className="station">
            {station.id && <p>ID: {station.id}</p>}
            {station.measurement_frequency && (
              <p>Messfrequenz: {station.measurement_frequency}</p>
            )}
            {station.reporting_obligations &&
              station.reporting_obligations.length > 0 && (
                <>
                  <p>Berichtspflichten: </p>
                  <ul key="reporting_obligations">
                    {station.reporting_obligations
                      .sort()
                      .map((reporting_obligation) =>
                        buildReportingObligation(reporting_obligation),
                      )}
                  </ul>
                </>
              )}
          </div>
        )}
        {measured_variables && measured_variables.length > 0 && (
          <>
            <p>Gemessene Variablen:</p>
            <ul>
              {measured_variables.map((variable) => (
                <li key={variable}>{variable}</li>
              ))}
            </ul>
          </>
        )}
        {methods && methods.length > 0 && (
          <>
            <div className="row">Methode:</div>
            <div className="variables">
              {methods.map((method) => (
                <div key={method} className="row">
                  {method}
                </div>
              ))}
            </div>
          </>
        )}
      </Fragment>
    )
  } else if (typeof type === 'object' && 'ChemicalCompound' in type) {
    const compound = type.ChemicalCompound
    const name = compound.name
    const synonyms = compound.synonyms
    const cas_rn = compound.cas_registry_number

    return (
      <Fragment key={label}>
        <h3>{label}</h3>
        <p>Name: {name}</p>
        {synonyms && synonyms.length > 0 && (
          <>
            <p>Synonyme:</p>
            <ul>
              {synonyms.map((synonym) => (
                <li key={synonym}>{synonym}</li>
              ))}
            </ul>
          </>
        )}
        {cas_rn && <p>CAS-Registriernummer: {cas_rn}</p>}
      </Fragment>
    )
  } else {
    return <h3 key={label}>{label}</h3>
  }
}

function buildReportingObligation(reporting_obligation: string) {
  const obligation = REPORTING_OBLIGATIONS[reporting_obligation]
  return (
    <li key={obligation.label}>
      <a href={obligation.href} title={obligation.label}>
        {obligation.label}
      </a>
    </li>
  )
}

function buildAlternative(alternative: Alternative) {
  if ('Language' in alternative) {
    const url = alternative.Language.url
    const language = alternative.Language.language

    return (
      <a className="link" href={url} key={url}>
        <FiExternalLink />
        Angebot in {language}
      </a>
    )
  } else {
    const title = alternative.Source.title
    const url = alternative.Source.url

    return (
      <a className="link" href={url} key={url}>
        <FiExternalLink /> Weitere Quelle: {title}
      </a>
    )
  }
}

const StyledAccordionItem = styled(AccordionItem)`
  display: flex;
  justify-content: center;
  flex-direction: column;
  border-bottom: 1px solid ${theme.colors['neutral-600']};

  &:first-child {
    border-top: 1px solid ${theme.colors['neutral-600']};
  }

  .accordion-button {
    display: flex;
    align-items: center;
    cursor: pointer;
    border: none;
    background: none;
    text-align: left;
    padding: ${theme.sizing['5']} 0;

    > h2 {
      ${theme.ty('display/md bold')}
      flex: 1;
      color: ${theme.colors['primary-900']};
      margin: 0;
    }

    > .chevron {
      transform: rotate(0deg);
      transition: transform 0.3s;
    }
  }
  .accordion-button[aria-expanded="true"] {
    padding-bottom: ${theme.sizing['5']};
    .chevron {
      transform: rotate(180deg);
    }
  }
    
  h3 {
    ${theme.ty('display/base bold')}
    color: ${theme.colors.black};
  }

  .accordion-panel {
    padding-bottom: ${theme.sizing['5']};
    padding-left: ${theme.sizing['5']};
  }
  .accordion-panel[aria-hidden="false"] {
    .tags {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      gap: ${theme.sizing['3']};
      padding-left: 0;
      > button {
        ${theme.ty('display/sm regular')}
        cursor: pointer;
        padding: ${theme.sizing['1.5']} ${theme.sizing['3']};
        background: ${theme.colors['neutral-200']};
        border: 1px solid ${theme.colors['neutral-300']};
        color: ${theme.colors['neutral-700']};
        display: flex;
        align-items: center;
        gap: ${theme.sizing['1.5']};  
      }
    }
      
    .alternatives {
      display: flex;
      flex-direction: column;
      gap: ${theme.sizing[`5`]};
    }

    .resources {
      display: flex;
      flex-direction: column;
      gap: ${theme.sizing[`5`]};

      span.label {
        display: flex;
        gap: ${theme.sizing[`3`]};
        align-items: center;
        font-weight: 700;
      }

      a.link {
        color: ${theme.colors['primary-900']};
        text-decoration: underline;
        display: flex;
        flex-direction: column;
        overflow-wrap: anywhere;

        > svg {
          min-width: 19px;
          stroke: ${theme.colors['primary-900']};
        }
        &:hover {
          text-decoration: none;
        }
      }
    }
  }
}`
