import express from 'express'
import { createProxyMiddleware } from 'http-proxy-middleware'

const app = express()

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');

  if (req.method === 'OPTIONS' && (req.path === '/search' || req.path === '/complete')) {
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Max-Age', '86400');

    res.sendStatus(200);
  } else {
    next();
  }
});

app.use(createProxyMiddleware({
  target: 'https://md.umwelt.info',
  // target: 'http://localhost:8081',
  changeOrigin: true,
}))

app.listen(3001)
