# Search-UI

## Setup

- run `npm install` to install all dependencies
- run `npm run proxy` to start the CORS proxy (requires port 3001 to be forwarded)
- run `npm start` to start the project in development mode (requires port 3000 to be forwarded)

## Contributing

In preparation of a new commit to this project, please run the following commands locally before check-in:

```console
> npm run prettier:write
> npm run type-check
> npm run lint
> npm run test
```

## Technologies

- **React**: A popular frontend library to create interactive UI.
- **styled-components**: A *CSS-in-JS* solution to create CSS with JS. This way we do not have CSS files and will just produce one single JS bundle that can be injected into any HTML document.
- **zustand**: A small state management library to make the UI interactive and handle navigation and API requests.
